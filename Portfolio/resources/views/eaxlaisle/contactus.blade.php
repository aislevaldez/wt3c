<!doctype html>
<html lang="en">
  <head>
  	<title>Aisle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">



	</head>
	<body>
	<section class="section">
		<div class="container">
			<!-- <div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Website menu #04</h2>
				</div>
			</div>-->
		</div> 
		<div class="container">
			<nav class="navbar navbar-expand-lg" >
		    <div class="container">
		    	<a class="navbar-brand" href="homepage.php">AISLE</a>

		        <div class="navbar-nav ml-auto mr-md-3">
		        	<li class="nav-item"><a href="/eaxlaisle/homepage" class="nav-link">Home</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/aboutme" class="nav-link" >About</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/login" class="nav-link" >Login</a></li> 
		          <li class="nav-item"><a href="#" class="nav-link" id="active">Contact</a></li>
				</div>

		      </div>

		  </nav>
  		</div>
	</section>

	<center>   
		<div class="container">   
                   <form class="form-horizontal" method="post">
                            <fieldset>
        
                                <div class="form-group">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="fname" name="name" type="text" placeholder="First Name" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="lname" name="name" type="text" placeholder="Last Name" class="form-control">
                                    </div>
                                </div>
        
                                <div class="form-group mt-3">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
                                    </div>
                                </div>
        
                                <div class="form-group mt-3">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                    <div class="col-md-8">
                                        <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
                                    </div>
                                </div>
        
                                <div class="form-group mt-3">
                                    <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                    <div class="col-md-8">
                                        <textarea class="form-control" id="message" name="message" placeholder="Enter your message." rows="7"></textarea>
                                    </div>
                                </div>
        
                                <div class="form-group mt-3">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn-sm" style="background-color: black; color:white;">Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
	</div>
</center>

    <script src="{{asset('js/main.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	</body>
</html>

