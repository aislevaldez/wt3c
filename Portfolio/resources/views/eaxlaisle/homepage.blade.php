<!doctype html>
<html lang="en">
  <head>
  	<title>Aisle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
	<!-- <link rel="stylesheet" href="style.css"> -->

	<style>
		body{
			/* background-image: url("/Portfolio/public/images/Aisle (1).png"); */
			background-image: url("{{asset('images/Aisle (1).png')}}");
		}	
		.g-started{
			position:absolute;
			bottom:100px;
			right:275px;
		}	 
	</style>

	</head>
	<body>
	<section class="section">
		<div class="container">
			<!-- <div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Website menu #04</h2>
				</div>
			</div>-->
		</div> 
		<div class="container">
			<nav class="navbar navbar-expand-lg" >
		    <div class="container">
		    	<a class="navbar-brand" href="homepage.php">AISLE</a>

		        <div class="navbar-nav ml-auto mr-md-3">
		        	<li class="nav-item"><a href="/eaxlaisle/homepage" class="nav-link" id="active">Home</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/aboutme" class="nav-link">About</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/login" class="nav-link">Login</a></li> 
		          <li class="nav-item"><a href="/eaxlaisle/contactus" class="nav-link">Contact</a></li>
				</div>

		      </div>

		  </nav>
  		</div>
	</section>

	<div class="content">
		<div class="g-started">
			<button type="submit" class="btn-sm" style="background-color: black; color:white;">Get Started</button>
		</div>
	</div>

    <script src="{{asset('js/main.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	</body>
</html>

