<!doctype html>
<html lang="en">
  <head>
  	<title>Aisle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

	<style>
		img{
		border-radius: 100px;
		background-color: #353535;
		}
		
		.logo{
			position: absolute;
			top: -20%; left: 50%;
			transform: translateX(-50%);

		}
		
		h2{
			margin-left: -.25em;
			font-weight: 1000;
		}
		
		.card{
		width : 50%;
		position: absolute;
		bottom: 25%; left: 50%;
		transform: translateX(-50%);
		box-shadow: 1px 5px 18px #888888;
		font-size: 12px;
		}
		
		.card-body{
		margin: 1.25em;
		}
		</style>

	</head>
	<body>
	<section class="section">
		<div class="container">
			<!-- <div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Website menu #04</h2>
				</div>
			</div>-->
		</div> 
		<div class="container">
			<nav class="navbar navbar-expand-lg" >
		    <div class="container">
		    	<a class="navbar-brand" href="homepage.php">AISLE</a>

		        <div class="navbar-nav ml-auto mr-md-3">
		        	<li class="nav-item"><a href="/eaxlaisle/homepage" class="nav-link">Home</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/aboutme" class="nav-link" >About</a></li>
		        	<li class="nav-item"><a href="#" class="nav-link"  id="active">Login</a></li> 
		          <li class="nav-item"><a href="/eaxlaisle/contactus" class="nav-link">Contact</a></li>
				</div>

		      </div>

		  </nav>
  		</div>
	</section>

	<div class="content">
					
		<div id="container-fluid">
		
		<div class="card">
			<div class="card-content">
				
					<p class="text-center"> <p>
				
				<div class="card-body">
					<form action="#" method="POST">
					<h2> Login</h2>

					<div class = "container-error"> 
						</div>
						<div class="form-group">
						<input type="text" class="form-control input-lg mb-3 mt-3" id="signin" name="username" required placeholder=" Username"/>
						</div>

					<div class="form-group">
						<input type="password" class="form-control input-lg mb-3" id="signin" name="password" required placeholder=" Password"/>
					</div>

				<p style ="margin-left:1em;">
				<a href ="#" style="text-decoration:none; color: #000957"> Forgot Password? </a>
				|
				<a href ="/eaxlaisle/registration" style="text-decoration:none; color: #000957"> Create Account </a>
				</p>

					<div id="login">
					<button type="submit" class="btn-sm" style="background-color: black; color:white;">Login</button>
					
					</div>
				</form>
			</div>
		</div>
		<div class="logo">
				<center><img src ="/images/img.png" style="width:100px"></center>
		</div>

		</div>

		</div>
		</div>

	</div>

    <script src="{{asset('js/main.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	</body>
</html>

