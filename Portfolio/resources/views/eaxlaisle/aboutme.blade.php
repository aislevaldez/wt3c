<!doctype html>
<html lang="en">
  <head>
  	<title>Aisle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

	<style>
		img{
			margin-left: 3em;
			margin-top: 3em;
			width: 300px;
		}
		.col-sm{
			margin-top: 2em;
			text-align: justify;
  			text-justify: inter-word;
			margin-right: 3em;
		}
	</style>

	</head>
	<body>
	<section class="section">
		<div class="container">
			<!-- <div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Website menu #04</h2>
				</div>
			</div>-->
		</div> 
		<div class="container">
			<nav class="navbar navbar-expand-lg" >
		    <div class="container">
		    	<a class="navbar-brand" href="homepage.php">AISLE</a>

		        <div class="navbar-nav ml-auto mr-md-3">
		        	<li class="nav-item"><a href="/eaxlaisle/homepage" class="nav-link">Home</a></li>
		        	<li class="nav-item"><a href="#" class="nav-link" id="active" >About</a></li>
		        	<li class="nav-item"><a href="/eaxlaisle/login" class="nav-link">Login</a></li> 
		          <li class="nav-item"><a href="/eaxlaisle/contactus" class="nav-link">Contact</a></li>
				</div>

		      </div>

		  </nav>
  		</div>
	</section>

	<div class="content">
		
		<div class="container">
		 <div class="row">
			<div class="col-sm-4">
			<image src ="{{asset('images/Aisle.png')}}" alt="Quotation">
			</div>
			<div class="col-sm">
			Hello there! 
			<br> <br>
			"In some ways, programming is like painting. You start with a blank canvas and certain basic raw materials. You use a combination of science, art, and craft to determine what to do with them." - Andrew Hunt
			<br> <br>
			I finished my high school at San Pedro National High School as one of the With Honors. 
			Currenty I'm taking up Bachelor of Science in Information Technology at Pangasinan State University Urdaneta City Campus.
			<br> <br>
			I'm a member of our shool publication back in high school. I write News and Editorial Articles. 
			I'm also a member of our school choir and tried to compete with different schools and even represent our Town in a Provincial Chorale Competition. 
			<br> <br>
			I don’t update this blog much, but there’s still lots of good, free content here, so feel free to dig around.
			</div>
		</div>`
</div>
	</div>

    <script src="{{asset('js/main.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	</body>
</html>

