<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/eaxlaisle/homepage', function () {
    return view('eaxlaisle/homepage');
});

Route::get('/eaxlaisle/contactus', function () {
    return view('eaxlaisle/contactus');
});

Route::get('/eaxlaisle/registration', function () {
    return view('eaxlaisle/registration');
});

Route::get('/eaxlaisle/login', function () {
    return view('eaxlaisle/login');
});

Route::get('/eaxlaisle/aboutme', function () {
    return view('eaxlaisle/aboutme');
});

