<?php
require_once 'config.php';

$permissions = ['email'];
if (isset($accessToken))
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		
		$oAuth2Client = $fb->getOAuth2Client();
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} 
	else 
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	
	
	if (isset($_GET['code'])) 
	{
		header('Location: ./');
	}
	
	
	try {
		$fb_response = $fb->get('/me?fields=name,first_name,last_name,email');
		$fb_user = $fb_response->getGraphUser();
		
		$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
		$_SESSION['fb_user_name'] = $fb_user->getProperty('name');

		
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Facebook API Error: ' . $e->getMessage();
		session_destroy();
		header("Location: ./");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK Error: ' . $e->getMessage();
		exit;
	}
} 
else 
{	
	$fb_login_url = $fb_helper->getLoginUrl('http://localhost/login/facebook-login-php/', $permissions);
}
?>


<?php
include('config2.php');

$login_button = '';

if(isset($_GET["code"]))
{

 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 if(!isset($token['error']))
 {
  $google_client->setAccessToken($token['access_token']);

  $_SESSION['access_token'] = $token['access_token'];

  $google_service = new Google_Service_Oauth2($google_client);

  $data = $google_service->userinfo->get();

  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }


 }
}

if(!isset($_SESSION['access_token']))
{
 $login_button = '<a href="'.$google_client->createAuthUrl().'"> Sign in with Google+ </a>';
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login FB / Google api script</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <script src="https://kit.fontawesome.com/021a3893c0.js" crossorigin="anonymous"></script>
  
  <style>
body{
  margin-top: 5em;
}

.btn-fb{
  margin-top: 10px;
  background-color:#3a63be;
  border-style: none;
  color: white;
  width:100%;
  padding: .5em;
  font-family: 'Montserrat' , sans-serif;
  font-weight: bold;
  font-size: 15px;
}

.btn-out{
  margin-top: 10px;
  background-color:#009900;
  border-style: none;
  color: white;
  width:100%;
  padding: .5em;
  font-family: 'Montserrat' , sans-serif;
  font-weight: bold;
  font-size: 15px;
}

.btn-g{
    margin-top: 10px;
  background-color:#c94130;
  border-style: none;
  color: white;
  padding: .5em;
  width:100%;
  font-family: 'Montserrat' , sans-serif;
  font-weight: bold;
  font-size: 15px;
}

p{
    margin-bottom:0;
    font-size: 25px;
    color:#3a63be;
    font-weight:bold;
    font-family: 'Montserrat' , sans-serif;

}
.container {
  margin: auto;
  width: 55%;
  border: 3px solid grey;
  padding: 10px;
}

.center {
  margin: auto;
  width: 55%;
}

a{
  text-decoration:none;
  color:white;
}


    </style>

</head>
<body>




<?php if(isset($_SESSION['fb_user_id']) ): ?>
	<div class="center">
            <div class="row">
                <div class="col"> <p> Name: <?php echo $_SESSION['fb_user_name']; ?> </p> </div>
                <div class="col"> <p> Year & Section: BSIT 3-C </p> </div>
                <div class="w-100"></div>
                <div class="col"> <p> March 3, 2022 </p></div>
                <div class="col"> <p> 1:49 AM </p></div>
            </div>
        </div>


        <div class="container">
        <div class="row">
            <div class="col">
                <button type="submit" class="btn-out" name="facebook">
             <span style="font-size: 2em;">
             <a href = 'logout.php'> Logout
             </a>
                </span>  
            </div>
        </div>
	</div>



	
			

<?php else: ?>

	<div class="center">
            <div class="row">
                <div class="col"> <p> Name: Aisle Lush S. Valdez </p> </div>
                <div class="col"> <p> Year & Section: BSIT 3-C </p> </div>
                <div class="w-100"></div>
                <div class="col"> <p> March 3, 2022 </p></div>
                <div class="col"> <p> 1:49 AM </p></div>
            </div>
        </div>


        <div class="container">
        <div class="row">
            <div class="col">
                <button type="submit" class="btn-fb" name="facebook">
             <span style="font-size: 2em;">
             <a href="<?php echo $fb_login_url;?>">
                <i class="fa fa-brands fa-facebook-f"></i> Login with Facebook 
             </a>
                </span>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button type="submit"  class="btn-g" name="google">				
                <span style="font-size: 2em; color:white;">
                <i class="fa fa-brands fa-google-plus-g"></i>  <?php echo $login_button;?>	
                </span> 
</a>
                </button>
            </div>
        </div>

        </div>	
<?php endif ?>
      
</body>
</html>
