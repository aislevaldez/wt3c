<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class MainController extends Controller
{
    function login (){
        return view('admin/login');
    }  

    function checklogin(Request $request){
        $request ->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        // $uname = $request-> input('username');
        // $password =  $request-> input('password');



        $user_data = array(
            'username' => $request-> get('username'),
            'password' => $request-> get('password')
        );


        // $user = array(
        //     'username' => DB::table('users')->get('username')->first(),
        //     'password' => DB::table('users')->get('password')->first()
        // );

        // if ($user_data == $user ){
        //     return view('/admin-dashboard',$data);
        // }


        echo Auth::attempt($user_data);
       
       
        if(Auth::attempt($user_data)){
            return redirect('/admin-dashboard');
        }
        else{
     
            // return back()->with('error', 'Wrong login details');
            return redirect('/admin-dashboard');
        }


    

    }

    function successlogin(){

        $data = array(
            'cover'=>DB::table('cover')->get(),
            'info'=>DB::table('data')->where('id','1')->get()
        );

        return view('admin/dashboard',$data);
    }

    function logout(){
        Auth::logout();
        return redirect('/admin-login');
    }

    

}


