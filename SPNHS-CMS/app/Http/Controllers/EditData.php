<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;


class EditData extends Controller
{

    function viewEditNews(){

        $slug = request()->segment(2);

        $data = array(
            'list'=>DB::table('news')->where('slug', $slug)->first()
        );

        return view('admin/editnews', $data);
    }


    function editNews(Request $request){
        $request -> validate([
            'title' => 'required',
            'content' => 'required',
        ]);

      
        if ($request->input('image') != NULL){

            $newImageName=time().'-'.$request->name . '.' .
            $request->image->extension();
            $request->image->move(public_path('images'),$newImageName);

            $last_edit = date("Y-m-d"); 

            $slug = Str::slug ($request-> input('title'));

            $id = request()->segment(4);

            $query = DB::table('news')
            ->where('id', $id)
            ->update([
                'title' => $request-> input('title'),
                'content'=> $request-> input('content'),
                'img' => $newImageName,
                'slug' => $slug,
                'last_edit' => $last_edit,
            ]);

        }

        else{

            $last_edit = date("Y-m-d"); 

            $slug = Str::slug ($request-> input('title'));

            $id = request()->segment(4);

            $query = DB::table('news')
            ->where('id', $id)
            ->update([
                'title'     => $request-> input('title'),
                'content'=> $request-> input('content'),
                'slug' => $slug,
                'last_edit' => $last_edit,
            ]);
        }
        
        if($query){
            return back()->with('success', 'Data edited');
        } else
            return back()->with('fail', 'Data not edited');
    }


    function viewEditEvent(){

        $slug = request()->segment(2);

        $data = array(
            'list'=>DB::table('event')->where('ev_slug', $slug)->first()
        );

        return view('admin/editevent', $data);
    }


    function editEvents(Request $request){
        $request -> validate([
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
            'stime' => 'required',
            'etime' => 'required|after:stime',
            'location' => 'required',
        ]);

      
        if ($request->image != NULL){

            $newImageName=time().'-'.$request->name . '.' .
            $request->image->extension();
            $request->image->move(public_path('images'),$newImageName);
    
            $slug = Str::slug ($request-> input('title')).($request-> input('date'));
    
            $id = request()->segment(4);

            $query = DB::table('event')
            ->where('ev_id', $id)
            ->update([
                'ev_title'=> $request-> input('title'),
                'ev_stime'=> $request-> input('stime'),
                'ev_etime'=> $request-> input('etime'),
                'ev_content'=> $request-> input('content'),
                'ev_img' => $newImageName,
                'ev_slug' => $slug,
                'ev_date' => $request-> input('date'),
                'ev_location' => $request-> input('location'),
            ]);

        }

        else{

            $slug = Str::slug ($request-> input('title'));
    
            $id = request()->segment(4);

            $query = DB::table('event')
            ->where('ev_id', $id)
            ->update([
                'ev_title'=> $request-> input('title'),
                'ev_stime'=> $request-> input('stime'),
                'ev_etime'=> $request-> input('etime'),
                'ev_content'=> $request-> input('content'),
                'ev_slug' => $slug,
                'ev_date' => $request-> input('date'),
                'ev_location' => $request-> input('location'),
            ]);
        }
        
        if($query){
            return back()->with('success', 'Data edited');
        } else
            return back()->with('fail', 'Data not edited');
    }



    
}
