<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class ViewData extends Controller
{
    function viewNews(){
        $data = array(
            'list'=>DB::table('news')->get()
        );
    
        return view('admin/news',$data);
    }

    function viewEvent(){

        $data = array(
            'list'=>DB::table('event')->get()
        );
        
        return view('admin/events',$data);
    }

    function viewAdmission(){
        $data = array(
            'jh'=>DB::table('program')->where('p_level', 'Junior High School')->get(),
            'sh'=>DB::table('program')->where('p_level', 'Senior High School')->get()
        );

        return view('admin/admission',$data);
    }

    function viewAdministration(){

        $data = array(
            'ht'=>DB::table('hteacher')->get(),
            'teacher'=>DB::table('teacher')->get(),
            'principal'=>DB::table('principal')->get()
        );
        return view('admin/administration',$data);
    }

    function viewContact(){

        $data = array(
            'email'=>DB::table('email')->get(),
            'number'=>DB::table('number')->get(),
            'address'=>DB::table('address')->get()
        );

        return view('admin/contact',$data);
    }

    function viewArchive(){

        $data = array(
            'about'=>DB::table('about')->get(),
        );

        return view('admin/archive', $data);
    }
    
}
