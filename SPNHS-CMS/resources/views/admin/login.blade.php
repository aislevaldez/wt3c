 <?php
    // require("include/dbconfig.php");
    // session_start();
    
    // if(isset($_POST['submit'])){
    //     $username = $_POST['username'];
    //     $password = $_POST['password'];
    //     $ref = 'user'; 

    //         $getdata = $database->getReference($ref)->getchild($username)->getValue();

    //         if($getdata['password'] == "$password"){
    //             $_SESSION['USERNAME'] = $getdata['username'];
    //             header("Location: home.php");
    //         }
    //         else{ 
    //             header("Location: index.php?err");
    //         }
    // }
?>

<!DOCTYPE html>
<html>
<head>
<title>Log In</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{
    margin: 0; padding: 0; border: 0; box-sizing: border-box;  
    background-image: url("images/bg2.png");
    background-repeat: no-repeat;
    background-size: cover;
    background-position-y: -100px}
    </style>
</head>
<body>

<div id="container-fluid">
   
	<div class="card">
		<div class="card-content">
			
				<p class="text-center"> <p>
		  
			<div class="card-body">
				
				<h2> Sign In</h2>

                <div class = "container-error">
                </div>
                @if(isset(Auth::user()->username))
                    <script>window.location ="/admin-dashboard"; </script>
                @endif

                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                        <strong> {{$message}} </strong>
                <div>
                @endif
                    
                @if(count($errors) > 0)
                <div class='alert alert-danger'>
                  <ul>
                      @foreach($errors->all() as $error)
                      <li> {{ $error}} </li>
                      @endforeach
                    </ul>
                </div>
                @endif

            <form method="post" action="{{ url('main/checklogin')}}">   
                {{ csrf_field()}}     
                <div class="form-group">
                    <input type="text" class="form-control input-lg" id="signin" name="username" placeholder="&#61447; Username"/>
                </div>

				<div class="form-group">
					<input type="password" class="form-control input-lg" id="signin" name="password" placeholder="&#61475; Password"/>
				</div>
				<div id="login">
				<input type="submit" class="btn-login btn-block btn-md"  name="submit" value="LOGIN"/>
				</div>
			</form>
		</div>
	</div>
	<div class="logo">
			<center><img src ="/images/spnhs.png" style="width:85px"></center>
            <p style="font-weight: 800; font-size:20px;"> Administrator </p>
	</div>

</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
<script src="https://kit.fontawesome.com/yourcode.js"></script>

</body>
</html>