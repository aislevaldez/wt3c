<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Admin</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="css/style.css">
   
    </head>

    <body oncontextmenu='return false' class='snippet-body'>
    <body id="body-pd">
    <header class="header" id="header">
        <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
        <div class="header_img"> <img src="https://www.pngmart.com/files/21/Admin-Profile-Vector-PNG-File.png" alt=""> </div>
    </header>
    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> <a href="#" class="nav_logo"> <img src ="images/spnhs.png" style="width:1.5em; background-color:white; border-radius:100%;"> <span class="nav_logo-name">SPNHS</span> </a>
                <div class="nav_list">

                    <a href="/admin-dashboard" class="nav_link" data-toggle="tooltip" data-placement="right" title="Dashboard"> <i class='bx bx-grid-alt nav_icon'></i> <span class="nav_name">Home</span></a>
                    <a href="/admin-news" class="nav_link" data-toggle="tooltip" data-placement="right" title="News"> <i class='bx bx-news nav_icon'></i> <span class="nav_name">News</span> </a>
                    <a href="/admin-event" class="nav_link" data-toggle="tooltip" data-placement="right" title="Events"> <i class='bx bx-calendar-event nav_icon'></i> <span class="nav_name">Events</span> </a>
                    <a href="/admin-admission" class="nav_link" data-toggle="tooltip" data-placement="right" title="Programs"> <i class='bx bx-bookmark nav_icon'></i> <span class="nav_name">Programs</span> </a>
                    <a href="#" class="nav_link active" data-toggle="tooltip" data-placement="right" title="Administration"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Administration</span> </a>
                    <a href="/admin-archive" class="nav_link" data-toggle="tooltip" data-placement="right" title="About"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">About</span> </a>
                    <a href="/admin-contact" class="nav_link" data-toggle="tooltip" data-placement="right" title="Contact"> <i class='bx bx-phone nav_icon'></i> <span class="nav_name">Contact</span> </a>

                    
                </div>

            </div> <a href="#" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
        </nav>
    </div>
    <!--Container Main start-->
    <div class="height-100 bg-light">
        <h4>Administration</h4>

        @if(Session::get('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @endif

        @if(Session::get('fail'))
            <div class="alert alert-danger">
                {{Session::get('fail')}}
            </div>
        @endif

        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">

        <form action="main/addprincipal" method="post" enctype="multipart/form-data"> 
        @csrf
        <div class="col-sm-12">      
            <label for="principal" class="col-sm-4 col-form-label">Principal</label> 
            <input type="text" class="form-control" id="principal" name="principal" value="{{ old ('principal') }}">

            <span style="color:red;"> @error ('principal'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="img" class="col-sm-4 col-form-label">Image</label> 
            <input type="file" class="form-control" id="image" name="image" value="{{ old ('image') }}">

            <span style="color:red;"> @error ('image'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="ranking" class="col-sm-4 col-form-label">Ranking</label> 
            <input type="text" class="form-control" id="ranking" name="ranking" value="{{ old ('ranking') }}">

            <span style="color:red;"> @error ('ranking'){{$message}} @enderror </span>
        </div>

        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Update </button>
        </div>
        </form>

            @if ($principal != null)
            <div class='container'>           
                @foreach ($principal as $principal)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$principal->name}} <br>
                        {{$principal->ranking}}
                        </p>  
                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div>       
            


        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">
        <form action="main/addhead" method="post" enctype="multipart/form-data"> 
        @csrf

        <div class="col-sm-12">      
            <label for="ht" class="col-sm-4 col-form-label">Head Teacher</label> 
            <input type="text" class="form-control" id="ht" name="ht" value="{{ old ('ht') }}">

            <span style="color:red;"> @error ('ht'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="img" class="col-sm-4 col-form-label">Image</label> 
            <input type="file" class="form-control" id="image" name="image" value="{{ old ('image') }}">

            <span style="color:red;"> @error ('image'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="ranking" class="col-sm-4 col-form-label">Ranking</label> 
            <input type="text" class="form-control" id="ranking" name="ranking" value="{{ old ('ranking') }}">

            <span style="color:red;"> @error ('ranking'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="subject" class="col-sm-4 col-form-label"> Subject </label> 
            <select name="subject" id="subject" class="form-control"> 
            <option selected disabled>Select</option>
            <option value="English">English</option>
            <option value="Filipino">Filipino</option>
            <option value="Mathematics">Mathematics</option>
            <option value="Science">Science</option>
            <option value="Araling Panlipunan">Araling Panlipunan</option>
            <option value="Technology and Livelihood Education (TLE)">Technology and Livelihood Education (TLE)</option>
            <option value="Music, Arts, Physical Education, and Health (MAPEH)">Music, Arts, Physical Education, and Health (MAPEH)</option>
            <option value=" Edukasyon sa Pagpapakatao (ESP)"> Edukasyon sa Pagpapakatao (ESP)</option>
            </select>
            <span style="color:red;"> @error ('subject'){{$message}} @enderror </span>
        </div>

        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
        </div>

        </form>

        @if ($ht != null)
            <div class='container'>           
                @foreach ($ht as $ht)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$ht->name}}
                        </p>    
                        {{$ht->ranking}} - {{$ht->subject}}
                    </div>
                    <div class="col">
                            <form action="/deleteHt/{{$ht->id}}" method="post" enctype="multipart/form-data"> 
                                @csrf
                            <button type="submit" class="btn btn-primary" style="width:100%;"><i class="fas fa-trash" id="trash"></i></button>
                            </form>
                                                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div> 


        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">
        <form action="main/addteacher" method="post" enctype="multipart/form-data"> 
        @csrf

    

        <div class="col-sm-12">      
            <label for="teacher" class="col-sm-4 col-form-label">Teacher</label> 
            <input type="text" class="form-control" id="teacher" name="teacher" value="{{ old ('teacher') }}">

            <span style="color:red;"> @error ('teacher'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="img" class="col-sm-4 col-form-label">Image</label> 
            <input type="file" class="form-control" id="image" name="image" value="{{ old ('image') }}">

            <span style="color:red;"> @error ('image'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="ranking" class="col-sm-4 col-form-label">Ranking</label> 
            <input type="text" class="form-control" id="ranking" name="ranking" value="{{ old ('ranking') }}">

            <span style="color:red;"> @error ('ranking'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="subject" class="col-sm-4 col-form-label"> Subject </label> 
            <select name="subject" id="subject" class="form-control"> 
            <option selected disabled>Select</option>
            <option value="English">English</option>
            <option value="Filipino">Filipino</option>
            <option value="Mathematics">Mathematics</option>
            <option value="Science">Science</option>
            <option value="Araling Panlipunan">Araling Panlipunan</option>
            <option value="Technology and Livelihood Education (TLE)">Technology and Livelihood Education (TLE)</option>
            <option value="Music, Arts, Physical Education, and Health (MAPEH)">Music, Arts, Physical Education, and Health (MAPEH)</option>
            <option value=" Edukasyon sa Pagpapakatao (ESP)"> Edukasyon sa Pagpapakatao (ESP)</option>
            </select>
            <span style="color:red;"> @error ('subject'){{$message}} @enderror </span>
        </div>


        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
        </div>

        </form>

        @if ($teacher != null)
            <div class='container'>           
                @foreach ($teacher as $teacher)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$teacher->name}}
                        </p>    
                        {{$teacher->ranking}} - {{$teacher->subject}}
                    </div>
                    <div class="col">
                            <form action="/deleteteacher/{{$teacher->id}}" method="post" enctype="multipart/form-data"> 
                                @csrf
                            <button type="submit" class="btn btn-primary" style="width:100%;"><i class="fas fa-trash" id="trash"></i></button>
                            </form>
                                                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div>



    </div>
    <!--Container Main end-->

    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src=''></script>
    <script type='text/javascript' src=''></script>
    
    <script type='text/Javascript'>document.addEventListener("DOMContentLoaded", function(event) {

    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId)

    // Validate that all variables exist
    if(toggle && nav && bodypd && headerpd){
    toggle.addEventListener('click', ()=>{
    // show navbar
    nav.classList.toggle('show')
    // change icon
    toggle.classList.toggle('bx-x')
    // add padding to body
    bodypd.classList.toggle('body-pd')
    // add padding to header
    headerpd.classList.toggle('body-pd')
    })
    }
    }

    showNavbar('header-toggle','nav-bar','body-pd','header')

    /*===== LINK ACTIVE =====*/
    const linkColor = document.querySelectorAll('.nav_link')

    function colorLink(){
    if(linkColor){
    linkColor.forEach(l=> l.classList.remove('active'))
    this.classList.add('active')
    }
    }
    linkColor.forEach(l=> l.addEventListener('click', colorLink))

    // Your code to run since DOM is loaded and ready
    });</script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
</body>
</html>