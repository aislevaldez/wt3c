<?php
    // require("include/dbconfig.php");
    // session_start();
    
    // if(isset($_POST['submit'])){
    //     $username = $_POST['username'];
    //     $password = $_POST['password'];
    //     $ref = 'user'; 

    //         $getdata = $database->getReference($ref)->getchild($username)->getValue();

    //         if($getdata['password'] == "$password"){
    //             $_SESSION['USERNAME'] = $getdata['username'];
    //             header("Location: home.php");
    //         }
    //         else{ 
    //             header("Location: index.php?err");
    //         }
    // }
?>

<!DOCTYPE html>
<html>
<head>
<title>Log In</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href=style.css rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{
        margin: 0; padding: 0; border: 0; box-sizing: border-box;  
        background-image: url("images/bgp2.png");
        background-repeat: no-repeat;
        background-size: cover;
        background-position-y: -70px}

        
        .logo{
            position: absolute;
            top: -50%; left: 50%;
            transform: translateX(-51%);
        }

        h2{
            margin-left: -.25em;
            font-weight: 1000;
        }

        .card{
        width : 30%;
        position: absolute;
        bottom: 30%; left: 50%;
        transform: translateX(-100%);
        box-shadow: 1px 5px 18px #888888;
        font-size: 12px;
        }

        .card-body{
        margin: 1.25em;
        }

        .btn-login{
        background-color: #f48b6f;
        border: none;
        color: white;
        width: 100%;
        border-radius: 20px;
        margin-top: .5em;
        font-weight: bolder;
        font-size: 12px;
        padding: 7px;

        }

        .btn-login:hover {
        background-color: #000999;
        color: white;   
        }

        .form-control{
        border-radius: 20px; 
        background-color: transparent;
        border: 2;
        margin-bottom: 1em;
        font-size: 12px;
        padding: 7px;
        font-weight: 500;
        font-family: 'Poppins', sans-serif;
        }

    </style>
</head>
<body>

<div id="container-fluid">
   
	<div class="card">
		<div class="card-content">
			
				<p class="text-center"> <p>
		  
			<div class="card-body">
				<form action="#" method="POST">
				<h2> Sign In</h2>

                <div class = "container-error">
                <?php 
                // if(isset($_GET['err'])){
                //             echo "<div class='alert alert-danger'>
                //                     Invalid Username or Password
                //                     </div>";
                // }
                ?>
                    </div>
				    <div class="form-group">
					<input type="text" class="form-control input-lg" id="signin" name="username" required placeholder="&#61447; Username"/>
				    </div>

				<div class="form-group">
					<input type="password" class="form-control input-lg" id="signin" name="password" required placeholder="&#61475; Password"/>
				</div>


				<div id="login">
				<input type="submit" class="btn-login btn-block btn-md"  name="submit" value="LOGIN"/>
				</div>
			</form>
		</div>
	</div>
	<!-- <div class="logo">
			<center><img src ="/images/spnhs.png" style="width:85px"></center>
            <p style="font-weight: 800; font-size:20px;"> Administrator </p>
	</div> -->

</div>

</div>
</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>
<script src="https://kit.fontawesome.com/yourcode.js"></script>

</body>
</html>