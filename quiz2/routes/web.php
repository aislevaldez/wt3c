<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('item/{itemno}/{name}/{price}', function ($itemno, $name, $price) {
    return view('item') -> with ('itemno', $itemno) -> with ('name', $name) -> with ('price', $price);
});

Route::get('customer/{custnum}/{name}/{address}/{age?}', function ($custnum, $name, $address, $age = 'undifined') {
    return view ('customer') -> with ('custnum', $custnum) ->with ('name', $name) -> with ('address', $address) -> with ('age', $age);
});

Route::get('order/{custnum}/{name}/{orderno}/{date}', function ($custnum, $name, $orderno, $date) {
    return "Customer No: ".$custnum." Name: ".$name." Order No: ".$orderno." Date: ".$date. "<br> Aisle Lush Santos Valdez - BSIT 3C";
});

Route::get('orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{quantity}',
function ($transno, $orderno, $itemid, $name, $price, $quantity) {
    return "Transaction No: ".$transno." Order No: ".$orderno."Item ID: ".$itemid." Name: "
    .$name." Price: ".$price." Quantity: ".$quantity." Total: ".$price * $quantity."<br> Aisle Lush Santos Valdez - BSIT 3C";
});
