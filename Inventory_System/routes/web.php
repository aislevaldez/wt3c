<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login', 'App\Http\Controllers\MainController@index');
Route::get('/sign-up', 'App\Http\Controllers\MainController@signup');
Route::post('/adduser', 'App\Http\Controllers\MainController@adduser');

Route::post('/dashboard', 'App\Http\Controllers\MainController@userlogin');
Route::get('/dashboard/{id}', 'App\Http\Controllers\MainController@dashboard');
Route::post('/products', 'App\Http\Controllers\MainController@addProducts');
Route::get('/home/{id}', 'App\Http\Controllers\MainController@viewhome');

Route::get('/users', 'App\Http\Controllers\MainController@user');