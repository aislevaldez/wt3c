<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MainController extends Controller
{
    function signup(){

        return view('signup');
    }

    function index(){

        return view('login');
    }

    function adduser(Request $request){
        $request -> validate([
            'uname' => 'required|unique:users,username|regex:/(^[a-zA-Z]+[a-zA-Z0-9\\-]*$)/u',
            'email' => 'required|email|unique:users,email',
            'pass' => 'required|min:8',
            'repass' => 'required|same:pass|min:8'
        ]);

        $query = DB::table('users')->insert([        
            'username'=> $request-> input('uname'),
            'email'=> $request-> input('email'),
            'password' =>  $request-> input('pass'),
        ]);

        if($query){
            return back()->with('success', 'Successfuly Registered');
        } else
            return back()->with('fail', 'Repeat Registration');
        // return $request -> input();
    }

    function userlogin(Request $request){ 
        $request -> validate([      
            'your_name' => 'required|regex:/(^[a-zA-Z]+[a-zA-Z0-9\\-]*$)/u',
            'your_pass' => 'required|min:8',
        ]);

        $uname = $request-> input('your_name');
        $pass = $request-> input('your_pass');

        $user = DB::table('users')->where('username',$uname)->
        where('password', $pass)->pluck('username');        

        if($user){    
              $var = str_replace("[", "", $user);
              $var2 = str_replace("]", "", $var);
              $var3 = str_replace('"', "", $var2);

              if($var3 == 'admin'){
                 return redirect('/dashboard/'.$var3);
              } else{
                return redirect('/home/'.$var3);
              }

            // return redirect()->route('dashboard',['id' => $id]);
           
            
        } else
            return back()->with('fail', 'Invalid Login Details');

    }

    
    function dashboard(){
              
        $uname = request()->segment(2);

        $user = DB::table('users')->where('username',$uname)->pluck('id');

            $var = str_replace("[", "", $user);
            $var2 = str_replace("]", "", $var);
            $var3 = str_replace('"', "", $var2);


        if($user){

            $data = array(
                'list' => DB::table('products')->get()
            );

            $data1 = array(
            
            );

            return view('dashboard',$data,$data1);
        }
        else return redirect('login');       
    }


    function addProducts(Request $request){

        $today = date("Y/m/d");

        $request -> validate([      
            'name' => 'required',
            'price' => 'required',
            'stocks' => 'required',
        ]);

       
        $query = DB::table('products')->insert([        
            'name'=> $request-> input('name'),
            'stock'=> $request-> input('stocks'),
            'price' =>  $request-> input('price'),
            'date' => $today
        ]);

        if($query){
            return back()->with('success', 'Added');
        } else{
            return back()->with('fail', 'Not Added');
        }

        

    }

    function viewhome(){
              
        $uname = request()->segment(2);

        $user = DB::table('users')->where('username',$uname)->pluck('id');

            $var = str_replace("[", "", $user);
            $var2 = str_replace("]", "", $var);
            $var3 = str_replace('"', "", $var2);


        if($user){

            $data = array(
                'list' => DB::table('products')->get()
            );

            $data1 = array(
            
            );

            return view('home',$data,$data1);
        }
        else return redirect('login');       
    }


    function user(){
              
    

            $data = array(
                'list' => DB::table('users')->get()
            );


            return view('users',$data);
          
    }

}
