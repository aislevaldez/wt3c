<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css" />
    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");

    </style>
    </head>

    <style>
        .circle {
        border-radius: 50%;
        width: 34px;
        height: 34px;
        padding-top: 4px;
        padding-left: 11px;
        background-color:#0275d8;
        color:white;
        font-weight:bold;
        }

        p{
            color:#0275d8;
            font-weight:bold;
        }

        </style>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="navbar-brand mt-2 mt-lg-0">
                <p> APPOINTMENT </P>
            </div>
 
            </div>

            <div class="d-flex align-items-center">


        
                <?php
                    $uname = request()->segment(2);
                    $firstStringCharacter = substr($uname, 0, 1);
                    $string = strtoupper($firstStringCharacter);
                   
                    ?> 
                <div class="circle"> {{ $string }} </div>

                <form action="/logout" method="post"> 
                    @csrf
                <button type="submit" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
                </form>
     
              
           
            </div>
 
        </div>

        </nav>


        <table id="tblUser">
            <thead>
                <th>ID</th>
                <th>Username</th>
                <th>Email</th>
                <th>password</th>
            </thead>
            <tbody>
    
                   @if ($list != NULL)
        @foreach ($list as $prod)
                        <tr>
                            <td> {{$prod->id}} </td>
                            <td> {{$prod->username}}</td>
                            <td> {{$prod->email}}</td>
                            <td> {{$prod->password}}</td>
                        </tr>
                
             @endforeach
        @endif
            </tbody>
        </table>

        

         


        </div>

            
        
        </div>

    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script>
jQuery(document).ready(function($) {
    $('#tblUser').DataTable();
} );
</script>

</html>