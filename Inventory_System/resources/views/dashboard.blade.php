<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css" />
    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");

    </style>
    </head>

    <style>
        .circle {
        border-radius: 50%;
        width: 34px;
        height: 34px;
        padding-top: 4px;
        padding-left: 11px;
        background-color:#0275d8;
        color:white;
        font-weight:bold;
        }

        p{
            color:#0275d8;
            font-weight:bold;
        }

        </style>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="navbar-brand mt-2 mt-lg-0">
                <p> APPOINTMENT </P>
            </div>
 
            </div>

            <div class="d-flex align-items-center">


        
                <?php
                    $uname = request()->segment(2);
                    $firstStringCharacter = substr($uname, 0, 1);
                    $string = strtoupper($firstStringCharacter);
                   
                    ?> 
                <div class="circle"> {{ $string }} </div>

                <form action="/logout" method="post"> 
                    @csrf
                <button type="submit" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
                </form>
     
              
           
            </div>
 
        </div>

        </nav>


        <div class="row">

        <div class="col-lg">
        <div class="container">
            @if(Session::get('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            @endif

            @if(Session::get('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
            @endif
            <h3> Inventory </h3>

            <form action="/products" method="post" enctype="multipart/form-data"> 

            @csrf
                <?php
                    $uname = request()->segment(2);
                ?>
                <input type="hidden" name="id" id="name" value="<?php echo $uname ?>" />

                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name ="name" value="{{ old ('name') }}">
                    <span style="color:red;"> @error ('name'){{$message}} @enderror </span>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Price</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name ="price" value="{{ old ('price') }}">
                    <span style="color:red;"> @error ('price'){{$message}} @enderror </span>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Stocks</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name ="stocks" value="{{ old ('stocks') }}">
                    <span style="color:red;"> @error ('stocks'){{$message}} @enderror </span>
                </div> 

                <button type="submit" class="btn btn-primary mb-5">Submit</button>

            </form>
            </div>
        </div>

        <div class="col-lg">

        <table id="tblUser">
            <thead>
                <th>ID</th>
                <th>Product</th>
                <th>Price</th>
                <th>Stock</th>
            </thead>
            <tbody>
    
                   @if ($list != NULL)
        @foreach ($list as $prod)
                        <tr>
                            <td> {{$prod->id}} </td>
                            <td> {{$prod->name}}</td>
                            <td> {{$prod->price}}</td>
                            <td> {{$prod->stock}}</td>
                        </tr>
                
             @endforeach
        @endif
            </tbody>
        </table>

        

        

        


        </div>

            
        
        </div>

    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script>
jQuery(document).ready(function($) {
    $('#tblUser').DataTable();
} );
</script>

</html>