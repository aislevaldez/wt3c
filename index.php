<!DOCTYPE html>
<head>
<title>Aisle Lush S. Valdez</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

    <style>

        body{
            margin: 0; padding: 0; border: 0; box-sizing: border-box;  
            font-family: 'Poppins', sans-serif;
        }

        p{
            font-weight: bold;
            font-size: 15px;
            font-family: 'Poppins', sans-serif;
        }

        #container{
            font-family: 'Poppins', sans-serif;
            min-height: 100vh;
        }

        .top{
            height: 35vh;
            background-color: #4897e2;
            position: relative;
        }

        .bottom{
            height: 50vh;
            background-color: white;
        }

        img{
            position: absolute;
            top: -30%; left: 50%;
            transform: translateX(-50%);
            border-radius: 100px; 
        }

        .card{
            width : 40%;
            position: absolute;
            bottom: -100%; left: 50%;
            transform: translateX(-50%);
            border-radius: 20px; 
            box-shadow: 1px 5px 18px #888888;
            font-family: 'Poppins', sans-serif;
            font-weight: bold;
            font-size: 12px;
            text-align: center;
           
        }

        .card-body{
            margin: 2em;
        }

    </style>

</head>
<body>

    

    <div id="container">

        <div class="top">      
            <div class="card">
                <div class="card-content" >

                        <p class="text-center"> <p>
                        <div class="card-body">
                            <h2 style="font-weight: bold; margin-top:.5em;"> Valdez, Aisle Lush Santos </h2>
                            <p> Bachelor of Science in Information Technology </p>
                            <p> Third Year - Block C </p>
                            <p> Elective 1 (Web Systems and Technologies 2) </p>
                            <p> Current Date & Time: March 1, 2022 11:56 AM </p>
                        </div>
                </div>
            
                <div class="logo">
                    <img src ="img.jpg" style="width:130px">
                </div>

            </div>
        </div>

        <div class="bottom"> 

        </div>
    </div>
   

</body>
</html>