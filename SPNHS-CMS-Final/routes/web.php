<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/admin-login', function () {
//     return view('/admin/login');
// });

// Route::get('/admin-dashboard', function () {
//     return view('/admin/dashboard');
// });


// Route::get('/admin-news', function () {
//     return view('/admin/news');
// });

Route::get('/try', function () {
    return view('/admin/try');
});

Route::get('/admin-login', 'App\Http\Controllers\MainController@login');
Route::get('/admin-dashboard', 'App\Http\Controllers\MainController@successlogin');
Route::post('main/checklogin', 'App\Http\Controllers\MainController@checklogin');
Route::get('main/logout', 'App\Http\Controllers\MainController@logout');
Route::post('main/addnews', 'App\Http\Controllers\AddData@addNews');
Route::get('/admin-news', 'App\Http\Controllers\ViewData@viewNews');
Route::get('/admin-editnews/{slug}', 'App\Http\Controllers\EditData@viewEditNews');
Route::post('/admin-editnews/main/editnews/{id}', 'App\Http\Controllers\EditData@editNews');
Route::post('deleteNews/{id}', 'App\Http\Controllers\DeleteData@deleteNews');
Route::get('/admin-event', 'App\Http\Controllers\ViewData@viewEvent');
Route::get('/admin-admission', 'App\Http\Controllers\ViewData@viewAdmission');
Route::get('/admin-contact', 'App\Http\Controllers\ViewData@viewContact');
Route::get('/admin-administration', 'App\Http\Controllers\ViewData@viewAdministration');
Route::get('/admin-archive', 'App\Http\Controllers\ViewData@viewArchive');
Route::post('main/addevents', 'App\Http\Controllers\AddData@addEvents');
Route::post('deleteEvent/{id}', 'App\Http\Controllers\DeleteData@deleteEvent');
Route::get('/admin-editevent/{slug}', 'App\Http\Controllers\EditData@viewEditEvent');
Route::post('/admin-editevent/main/editevents/{id}', 'App\Http\Controllers\EditData@editEvents');
Route::post('main/addemail', 'App\Http\Controllers\AddData@addEmail');
Route::post('main/addnumber', 'App\Http\Controllers\AddData@addNumber');
Route::post('main/addaddress', 'App\Http\Controllers\AddData@addAddress');
Route::post('deleteEmail/{id}', 'App\Http\Controllers\DeleteData@deleteEmail');
Route::post('deleteNumber/{id}', 'App\Http\Controllers\DeleteData@deleteNumber');
Route::post('deleteAddress/{id}', 'App\Http\Controllers\DeleteData@deleteAddress');
Route::post('main/addabout', 'App\Http\Controllers\AddData@addAbout');
Route::post('main/addprogram', 'App\Http\Controllers\AddData@addProgram');
Route::post('deleteProgram/{id}', 'App\Http\Controllers\DeleteData@deleteProgram');
Route::post('main/addcover', 'App\Http\Controllers\AddData@addCover');
Route::post('main/adddata', 'App\Http\Controllers\AddData@addData');
Route::post('main/addteacher', 'App\Http\Controllers\AddData@addTeacher');
Route::post('main/addhead', 'App\Http\Controllers\AddData@addHead');
Route::post('main/addprincipal', 'App\Http\Controllers\AddData@addPrincipal');
Route::post('deleteHt/{id}', 'App\Http\Controllers\DeleteData@deleteHt');
Route::post('deleteteacher/{id}', 'App\Http\Controllers\DeleteData@deleteTeacher');
Route::get('/admin-messages', 'App\Http\Controllers\ViewData@viewMessages');
Route::post('forms/read', 'App\Http\Controllers\EditData@status');
Route::get('/admin-admin', 'App\Http\Controllers\ViewData@viewAdmin');
Route::post('/add-admin', 'App\Http\Controllers\AddData@addAdmin');

//user
Route::get('/', 'App\Http\Controllers\UserController@viewHome');
Route::get('/news', 'App\Http\Controllers\UserController@viewNews');
Route::get('/events', 'App\Http\Controllers\UserController@viewEvents');
Route::get('/about', 'App\Http\Controllers\UserController@viewAbout');
Route::get('/contact', 'App\Http\Controllers\UserController@viewContact');
Route::get('/courses', 'App\Http\Controllers\UserController@viewCourses');
Route::get('/administration', 'App\Http\Controllers\UserController@viewAdministration');
Route::get('/{id}', 'App\Http\Controllers\UserController@viewSnews');
Route::get('event/{slug}', 'App\Http\Controllers\UserController@viewSevent');
Route::post('forms/contact', 'App\Http\Controllers\UserController@message');