<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class DeleteData extends Controller
{
    function deleteNews(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM news WHERE id = ?', [$id]);

        if($query){
            return back()->with('success', 'Data deleted');
        } else
            return back()->with('fail', 'Data not deleted');
    }

    function deleteEvent(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM event WHERE ev_id = ?', [$id]);

        if($query){
            return back()->with('success', 'Data deleted');
        } else
            return back()->with('fail', 'Data not deleted');
    }

    function deleteEmail(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM email WHERE e_id = ?', [$id]);

        if($query){
            return back()->with('success', 'Email deleted');
        } else
            return back()->with('fail', 'Email not deleted');
    }

    function deleteNumber(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM number WHERE num_id = ?', [$id]);

        if($query){
            return back()->with('success', 'Number deleted');
        } else
            return back()->with('fail', 'Number not deleted');
    }

    function deleteAddress(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM address WHERE add_id = ?', [$id]);

        if($query){
            return back()->with('success', 'Address deleted');
        } else
            return back()->with('fail', 'Address not deleted');
    }

    function deleteProgram(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM program WHERE p_id = ?', [$id]);

        if($query){
            return back()->with('success', 'Program deleted');
        } else
            return back()->with('fail', 'Program not deleted');
    }

    function deleteHt(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM hteacher WHERE id = ?', [$id]);

        if($query){
            return back()->with('success', 'Deleted');
        } else
            return back()->with('fail', 'Not deleted');
    }


    function deleteTeacher(){
        $id = request()->segment(2);
        $query = DB::delete('DELETE FROM teacher WHERE id = ?', [$id]);

        if($query){
            return back()->with('success', 'Deleted');
        } else
            return back()->with('fail', 'Not deleted');
    }
}
