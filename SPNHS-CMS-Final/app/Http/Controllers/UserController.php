<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class UserController extends Controller
{
    function viewHome(){
        $event = DB::table('event')->get();
        $teacher = DB::table('teacher')->get();
        $data = array(
            'teacher'=> $teacher->count(),
            'cover'=>DB::table('cover')->get(),
            'info'=>DB::table('data')->where('id','1')->get(), 
            'countev'=> $event->count(),
            'news'=>DB::table('news')->orderBy('date', 'DESC')->take(3)->get(),
            'event'=>DB::table('event')->orderBy('ev_date', 'DESC')->take(3)->get(),
            'about'=>DB::table('about')->get(),
            'history'=>DB::table('about')->get(),
        );
        return view('user/home',$data);
    }

    function viewNews(){

        $data = array(
        'news'=>DB::table('news')->orderBy('date', 'DESC')->get(),
        );
        return view('user/news',$data);
    }

    function viewEvents(){
        $data = array(
        'event'=>DB::table('event')->orderBy('ev_date', 'DESC')->get(),
        );
        return view('user/events',$data);
    }

    function viewAbout(){
        $teacher = DB::table('teacher')->get();

        $event = DB::table('event')->get();

        $data = array(
            'about'=>DB::table('about')->get(),
            'info'=>DB::table('data')->where('id','1')->get(), 
            'countev'=> $event->count(),
            'teacher'=> $teacher->count(),
            'history'=>DB::table('about')->get(),
            'mission'=>DB::table('about')->pluck("a_mission")->first(),
            'vision'=>DB::table('about')->pluck("a_vision")->first(),
            'core'=>DB::table('about')->pluck("a_corevalues")->first(),
            'img'=>DB::table('about')->pluck("a_img")->first(),
        );
        return view('user/about',$data);
    }

    function viewContact(){

        $event = DB::table('event')->get();

        $data = array(
            'email'=>DB::table('email')->get(),
            'address'=>DB::table('address')->first(),
            'number'=>DB::table('number')->get(),
        );
        return view('user/contact',$data);
    }

    function viewCourses(){

       
        $data = array(
            'hs'=>DB::table('program')->where('p_level', "Junior High School")->get(),
            'sh'=>DB::table('program')->where('p_level', "Senior High School")->get(),
            'img'=>DB::table('about')->first(),
        );
        return view('user/courses',$data);
    }

    function viewAdministration(){

       
        $data = array(
            'ht'=>DB::table('hteacher')->get(),
            'teacher'=>DB::table('teacher')->get(),
            'principal'=>DB::table('principal')->first()
        );
        return view('user/administration',$data);
    }

    function viewSnews(){
        $slug = request()->segment(1);

        $data = array(
            'list'=>DB::table('news')->where('slug', $slug)->first(),
            'news'=>DB::table('news')->orderBy('date', 'DESC')->take(3)->get()
        );
        return view('user/snews',$data);
    }

    function viewSevent(){
        $slug = request()->segment(2);

        $data = array(
            'event'=>DB::table('event')->where('ev_slug', $slug)->first(),
            'list'=>DB::table('event')->orderBy('ev_date', 'DESC')->take(3)->get()
        );
        return view('user/sevent',$data);
    }

    function message(Request $request){
        $request -> validate([
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);

        $today = date("Y-m-d");

        $query = DB::table('messages')->insert([
            'name'=> $request-> input('name'),
            'email'=> $request-> input('email'),
            'content' => $request-> input('message'),  
            'date' => $today,
        ]);

        if($query){
            return back()->with('success', 'Sent');
        } else
            return back()->with('fail', 'Not Sent');
    }
}
