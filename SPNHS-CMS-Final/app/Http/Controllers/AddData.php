<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class AddData extends Controller
{
    function addNews(Request $request){

        $request -> validate([
            'title' => 'required',
            'content' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image' => 'required',
        ]);

      
        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);

        $today = date("Y-m-d"); 

        $slug = Str::slug ($request-> input('title'));

        $query = DB::table('news')->insert([
            'title'=> $request-> input('title'),
            'content'=> $request-> input('content'),
            'img' => $newImageName,
            'slug' => $slug,
            'date' => $today,
        ]);

        if($query){
            return back()->with('success', 'Data inserted');
        } else
            return back()->with('fail', 'Data not inserted');
        // return $request -> input();
    }

    function addEvents(Request $request){
        $request -> validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required',
            'date' => 'required|after:today',
            'stime' => 'required',
            'etime' => 'required|after:stime',
            'location' => 'required',
        ]);

        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);

        $slug = Str::slug ($request-> input('title')).($request-> input('date'));

        $query = DB::table('event')->insert([
            'ev_title'=> $request-> input('title'),
            'ev_stime'=> $request-> input('stime'),
            'ev_etime'=> $request-> input('etime'),
            'ev_content'=> $request-> input('content'),
            'ev_img' => $newImageName,
            'ev_slug' => $slug,
            'ev_date' => $request-> input('date'),
            'ev_location' => $request-> input('location'),
        ]);

        if($query){
            return back()->with('success', 'Data inserted');
        } else
            return back()->with('fail', 'Data not inserted');
    }

    function addEmail(Request $request){
        $request -> validate([
            'email' => 'required|email',
        ]);

        $query = DB::table('email')->insert([
            'e_email'=> $request-> input('email'),
        ]);

        if($query){
            return back()->with('success', 'Email inserted');
        } else
            return back()->with('fail', 'Email not inserted');
    }

    function addNumber(Request $request){
        $request -> validate([
            'phone' => 'required|regex:([0-9])|max:11|min:11',
        ]);

        $query = DB::table('number')->insert([
            'num_number'=> $request-> input('phone'),
        ]);

        if($query){
            return back()->with('success', 'Number inserted');
        } else
            return back()->with('fail', 'Number not inserted');
    }

    function addAddress(Request $request){
        $request -> validate([
            'street' => 'required',
            'city' => 'required',
            'province' => 'required',
            'zip' => 'required|regex:([0-9])|max:4|min:4',
        ]);

        $query = DB::table('address')->insert([
            'add_street' => $request-> input('street'),
            'add_city' => $request-> input('city'),
            'add_province' => $request-> input('province'),
            'add_zip' => $request-> input('zip'),
        ]);

        if($query){
            return back()->with('success', 'Address inserted');
        } else
            return back()->with('fail', 'Address not inserted');
    }
    
    function addAbout(Request $request){

        if ($request->image!= NULL){
            $newImageName=time().'-'.$request->name . '.' .
            $request->image->extension();
            $request->image->move(public_path('images'),$newImageName);

            $query = DB::table('about')->delete();

        $query = DB::table('about')->insert([
            'a_mission' => $request-> input('mission'),
            'a_corevalues' => $request-> input('cvalues'),
            'a_vision' => $request-> input('vision'),
            'a_why' => $request-> input('whyus'),
            'a_history' => $request-> input('his'),
            'a_img' => $newImageName,
        ]);


        } else{
            $id = $request-> input('id');
            $query = DB::table('about')
            ->where('a_id', $id)
            ->update([
                'a_mission' => $request-> input('mission'),
                'a_corevalues' => $request-> input('cvalues'),
                'a_vision' => $request-> input('vision'),
                'a_why' => $request-> input('whyus'),
                'a_history' => $request-> input('his'),
            ]);
        }
       
        
        if($query){
            return back()->with('success', 'Updated');
        } else
            return back()->with('fail', 'Not Updated');
    }

    function addProgram(Request $request){

        $request -> validate([
            'program' => 'required',
            'level' => 'required',
        ]);
       
        $query = DB::table('program')->insert([
            'p_name' => $request-> input('program'),
            'p_level' => $request-> input('level'),
          
        ]);

        if($query){
            return back()->with('success', 'Program Added');
        } else
            return back()->with('fail', 'Program Not Added');
    }

    function addCover(Request $request){
        $request -> validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image' => 'required',
        ]);

      
        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);

        $query = DB::table('cover')->delete();

        $query = DB::table('cover')->insert([
            'c_img' => $newImageName,
        ]);

        if($query){
            return back()->with('success', 'Updated');
        } else
            return back()->with('fail', 'Not Updated');
    }

    function addData(Request $request){

        $request -> validate([
            'students' => 'required',
        ]);

        $query = DB::table('data')->delete();
       
        $query = DB::table('data')->insert([
            'd_students' => $request-> input('students'),
        ]);

        if($query){
            return back()->with('success', 'Updated');
        } else
            return back()->with('fail', 'Not Updated');
    }
  
    function addTeacher(Request $request){

        $request -> validate([
            'teacher' => 'required',
            'ranking' => 'required',
            'subject' => 'required',
            'image' =>'required',
        ]);

        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);
       
        $query = DB::table('teacher')->insert([
            'name' => $request-> input('teacher'),
            'ranking' => $request-> input('ranking'),
            'subject' => $request-> input('subject'),
            'img' => $newImageName,
        ]);

        if($query){
            return back()->with('success', 'Teacher Added');
        } else
            return back()->with('fail', 'Teacher Not Added');
    }
 
    function addHead(Request $request){

        $request -> validate([
            'ht' => 'required',
            'ranking' => 'required',
            'subject' => 'required',
            'image' =>'required',
        ]);

        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);

        $query = DB::table('hteacher')->insert([
            'name' => $request-> input('ht'),
            'ranking' => $request-> input('ranking'),
            'subject' => $request-> input('subject'),
            'img' => $newImageName,
        ]);

        if($query){
            return back()->with('success', 'Head Teacher Added');
        } else
            return back()->with('fail', 'Head Teacher Not Added');
    }
    
    function addPrincipal(Request $request){

        $request -> validate([
            'principal' => 'required',
            'ranking' => 'required',
            'image' =>'required',
        ]);


        $newImageName=time().'-'.$request->name . '.' .
        $request->image->extension();
        $request->image->move(public_path('images'),$newImageName);


        $query = DB::table('principal')->delete();
       
        $query = DB::table('principal')->insert([
            'img' => $newImageName,
            'name' => $request-> input('principal'),
            'ranking' => $request-> input('ranking'),
        ]);

        if($query){
            return back()->with('success', 'Updated');
        } else
            return back()->with('fail', 'Not Updated');
    }

    function addAdmin(Request $request){

        $request -> validate([
            'username' => 'required',
            'password' => 'required',
            'repassword' =>'required|same:password',
        ]);


      
        $query = DB::table('users')->insert([
            'username' => $request-> input('username'),
            'password' => $request-> input('password'),        
        ]);

        if($query){
            return back()->with('success', 'Added');
        } else
            return back()->with('fail', 'Not Added');
    }
}
