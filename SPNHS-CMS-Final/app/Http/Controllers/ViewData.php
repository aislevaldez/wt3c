<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class ViewData extends Controller
{
    function viewNews(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'list'=>DB::table('news')->get(),
            'notif' => $notifi->count(),
        );
    
        return view('admin/news',$data);
    }

    function viewEvent(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'list'=>DB::table('event')->get(),
            'notif' => $notifi->count(),
        );
        
        return view('admin/events',$data);
    }

    function viewAdmission(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'jh'=>DB::table('program')->where('p_level', 'Junior High School')->get(),
            'sh'=>DB::table('program')->where('p_level', 'Senior High School')->get(),
            'notif' => $notifi->count(),
        );

        return view('admin/admission',$data);
    }

    function viewAdministration(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'ht'=>DB::table('hteacher')->get(),
            'teacher'=>DB::table('teacher')->get(),
            'principal'=>DB::table('principal')->get(),
            'notif' => $notifi->count(),
        );
        return view('admin/administration',$data);
    }

    function viewContact(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'email'=>DB::table('email')->get(),
            'number'=>DB::table('number')->get(),
            'address'=>DB::table('address')->get(),
            'notif' => $notifi->count(),
        );

        return view('admin/contact',$data);
    }

    function viewArchive(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'about'=>DB::table('about')->get(),
            'notif' => $notifi->count(),
        );

        return view('admin/archive', $data);
    }

    function viewMessages(){

        $notifi=DB::table('messages')->where('status',0)->get();
       

        $data = array(
            'messages'=>DB::table('messages')->orderBy('id','DESC')->get(),
            'notif' => $notifi->count(),
        );

        $query = DB::table('messages')
        ->update([      
            'status' => 1,
        ]);

        return view('admin/messages', $data);

        
    }

    function viewAdmin(){
        $notifi=DB::table('messages')->where('status',0)->get();

        $data = array(
            'list'=>DB::table('users')->get(),
            'notif' => $notifi->count(),
        );

        return view('admin/admin', $data);
    }
    
}
