<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Admin</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="css/style.css">
   
    </head>

    <body oncontextmenu='return false' class='snippet-body'>
    <body id="body-pd">
    <header class="header" id="header">
        <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
        <div class="header_img"> <img src="https://www.pngmart.com/files/21/Admin-Profile-Vector-PNG-File.png" alt=""> </div>
    </header>
    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> <a href="#" class="nav_logo"> <img src ="images/spnhs.png" style="width:1.5em; background-color:white; border-radius:100%;"> <span class="nav_logo-name">SPNHS</span> </a>
                <div class="nav_list">
                <a href="/admin-dashboard" class="nav_link" data-toggle="tooltip" data-placement="right" title="Dashboard"> <i class='bx bx-grid-alt nav_icon'></i> <span class="nav_name">Home</span></a>
                    <a href="/admin-news" class="nav_link" data-toggle="tooltip" data-placement="right" title="News"> <i class='bx bx-news nav_icon'></i> <span class="nav_name">News</span> </a>
                    <a href="/admin-event" class="nav_link" data-toggle="tooltip" data-placement="right" title="Events"> <i class='bx bx-calendar-event nav_icon'></i> <span class="nav_name">Events</span> </a>
                    <a href="/admin-admission" class="nav_link" data-toggle="tooltip" data-placement="right" title="Programs"> <i class='bx bx-bookmark nav_icon'></i> <span class="nav_name">Programs</span> </a>
                    <a href="/admin-administration" class="nav_link" data-toggle="tooltip" data-placement="right" title="Administration"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Administration</span> </a>
                    <a href="/admin-archive" class="nav_link" data-toggle="tooltip" data-placement="right" title="About"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">About</span> </a>
                    <a href="/admin-contact" class="nav_link active" data-toggle="tooltip" data-placement="right" title="Contact"> <i class='bx bx-phone nav_icon'></i> <span class="nav_name">Contact</span> </a>
                    <?php if($notif != 0)
                    echo '<a href="/admin-messages" class="nav_link" data-toggle="tooltip" data-placement="right" title="Messages"> <i class="bx bx-message nav_icon" style="color:red"></i> <span class="nav_name">Messages</span> </a>';
                    else echo '<a href="/admin-messages" class="nav_link" data-toggle="tooltip" data-placement="right" title="Messages"> <i class="bx bx-message nav_icon"></i> <span class="nav_name">Messages</span> </a>';
                    ?>
                    <a href="/admin-admin" class="nav_link" data-toggle="tooltip" data-placement="right" title="Admin"> <i class='bx bx-user-pin nav_icon'></i> <span class="nav_name">Admin</span> </a>
                </div>

            </div> <a href="main/logout" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
        </nav>
    </div>
    <!--Container Main start-->
    <div class="height-100 bg-light">
        <h4>Contact</h4>

        @if(Session::get('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @endif

        @if(Session::get('fail'))
            <div class="alert alert-danger">
                {{Session::get('fail')}}
            </div>
        @endif

        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">

        <form action="main/addemail" method="post" enctype="multipart/form-data"> 
        @csrf
        <div class="col-sm-12">      
            <label for="email" class="col-sm-4 col-form-label">Email</label> 
            <input type="email" class="form-control" id="email" name="email" value="{{ old ('email') }}">

            <span style="color:red;"> @error ('email'){{$message}} @enderror </span>
        </div>

        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
        </div>
        </form>

            @if ($email != null)
            <div class='container'>           
                @foreach ($email as $email)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$email->e_email}}
                        </p>    
                    </div>
                    <div class="col">
                            <form action="/deleteEmail/{{$email->e_id}}" method="post" enctype="multipart/form-data"> 
                                @csrf
                            <button type="submit" class="btn btn-primary" style="width:100%;"><i class="fas fa-trash" id="trash"></i></button>
                            </form>
                                                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div>       
            


        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">
        <form action="main/addnumber" method="post" enctype="multipart/form-data"> 
        @csrf

        <div class="col-sm-12">      
            <label for="title" class="col-sm-4 col-form-label">Phone</label> 
            <input type="text" class="form-control" id="phone" name="phone" value="{{ old ('phone') }}">

            <span style="color:red;"> @error ('phone'){{$message}} @enderror </span>
        </div>
        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
        </div>

        </form>

        @if ($number != null)
            <div class='container'>           
                @foreach ($number as $number)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$number->num_number}}
                        </p>    
                    </div>
                    <div class="col">
                            <form action="/deleteNumber/{{$number->num_id}}" method="post" enctype="multipart/form-data"> 
                                @csrf
                            <button type="submit" class="btn btn-primary" style="width:100%;"><i class="fas fa-trash" id="trash"></i></button>
                            </form>
                                                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div> 


        <div class="card">
        <div class="card-body" id="card-center">
        <div class="container">
        <form action="main/addaddress" method="post" enctype="multipart/form-data"> 
        @csrf
        <div class="col-sm-12">      
            <label for="street" class="col-sm-4 col-form-label">Street</label> 
            <input type="text" class="form-control" id="street" name="street" value="{{ old ('street') }}">

            <span style="color:red;"> @error ('street'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="city" class="col-sm-4 col-form-label">City</label> 
            <input type="text" class="form-control" id="city" name="city" value="{{ old ('city') }}">

            <span style="color:red;"> @error ('city'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="province" class="col-sm-4 col-form-label">Province</label> 
            <input type="text" class="form-control" id="province" name="province" value="{{ old ('province') }}">

            <span style="color:red;"> @error ('province'){{$message}} @enderror </span>
        </div>


        <div class="col-sm-12">      
            <label for="zip" class="col-sm-4 col-form-label">ZIP</label> 
            <input type="text" class="form-control" id="zip" name="zip" value="{{ old ('zip') }}">

            <span style="color:red;"> @error ('zip'){{$message}} @enderror </span>
        </div>



        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
        </div>

        </form>

        @if ($address != null)
            <div class='container'>           
                @foreach ($address as $address)
                <div class="row">
                    <div class="col-sm-11"> 
                        <p style="font-weight:bold;">
                        {{$address->add_street}}, {{$address->add_city}}, {{$address->add_province}} {{$address->add_zip}}
                        </p>    
                    </div>
                    <div class="col">
                            <form action="/deleteAddress/{{$address->add_id}}" method="post" enctype="multipart/form-data"> 
                                @csrf
                            <button type="submit" class="btn btn-primary" style="width:100%;"><i class="fas fa-trash" id="trash"></i></button>
                            </form>
                                                        
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>                                    
        </div>  
        </div>


    </div>
    <!--Container Main end-->

    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src=''></script>
    <script type='text/javascript' src=''></script>
    
    <script type='text/Javascript'>document.addEventListener("DOMContentLoaded", function(event) {

    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId)

    // Validate that all variables exist
    if(toggle && nav && bodypd && headerpd){
    toggle.addEventListener('click', ()=>{
    // show navbar
    nav.classList.toggle('show')
    // change icon
    toggle.classList.toggle('bx-x')
    // add padding to body
    bodypd.classList.toggle('body-pd')
    // add padding to header
    headerpd.classList.toggle('body-pd')
    })
    }
    }

    showNavbar('header-toggle','nav-bar','body-pd','header')

    /*===== LINK ACTIVE =====*/
    const linkColor = document.querySelectorAll('.nav_link')

    function colorLink(){
    if(linkColor){
    linkColor.forEach(l=> l.classList.remove('active'))
    this.classList.add('active')
    }
    }
    linkColor.forEach(l=> l.addEventListener('click', colorLink))

    // Your code to run since DOM is loaded and ready
    });</script>
</body>
</html>