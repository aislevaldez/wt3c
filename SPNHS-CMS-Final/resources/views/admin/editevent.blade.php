<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Admin</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src=''></script>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
        :root {
            --header-height: 3rem;
            --nav-width: 68px;
            --first-color: #337DEF;
            --first-color-light: #F5F5F5;
            --white-color: #F5F5F5;
            --body-font: 'Nunito', sans-serif;
            --normal-font-size: 1rem;
            --z-fixed: 100
        }

        *,
        ::before,
        ::after {
            box-sizing: border-box
        }

        body {
            position: relative;
            margin: var(--header-height) 0 0 0;
            padding: 0 1rem;
            font-family: var(--body-font);
            font-size: var(--normal-font-size);
            transition: .5s
        }

        a {
            text-decoration: none
        }

        .header {
            width: 100%;
            height: var(--header-height);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0 1rem;
            background-color: var(--white-color);
            z-index: var(--z-fixed);
            transition: .5s
        }

        .header_toggle {
            color: var(--first-color);
            font-size: 1.5rem;
            cursor: pointer
        }

        .header_img {
            width: 35px;
            height: 35px;
            display: flex;
            justify-content: center;
            border-radius: 50%;
            overflow: hidden
        }

        .header_img img {
            width: 40px
        }

        .l-navbar {
            position: fixed;
            top: 0;
            left: -30%;
            width: var(--nav-width);
            height: 100vh;
            background-color: var(--first-color);
            padding: .5rem 1rem 0 0;
            transition: .5s;
            z-index: var(--z-fixed)
        }

        .nav {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            overflow: hidden
        }

        .nav_logo,
        .nav_link {
            display: grid;
            grid-template-columns: max-content max-content;
            align-items: center;
            column-gap: 1rem;
            padding: .5rem 0 .5rem 1.5rem
        }

        .nav_logo {
            margin-bottom: 2rem
        }

        .nav_logo-icon {
            font-size: 1.25rem;
            color: var(--white-color)
        }

        .nav_logo-name {
            color: var(--white-color);
            font-weight: 700
        }

        .nav_link {
            position: relative;
            color: var(--first-color-light);
            margin-bottom: 1.5rem;
            transition: .3s
        }

        .nav_link:hover {
            color: var(--white-color)
        }

        .nav_icon {
            font-size: 1.25rem
        }

        .show {
            left: 0
        }

        .body-pd {
            padding-left: calc(var(--nav-width) + 1rem)
        }

        .active {
            color: var(--white-color)
        }

        .active::before {
            content: '';
            position: absolute;
            left: 0;
            width: 2px;
            height: 32px;
            background-color: var(--white-color)
        }

        .height-100 {
            height: 100vh
        }

        @media screen and (min-width: 768px) {
            body {
                margin: calc(var(--header-height) + 1rem) 0 0 0;
                padding-left: calc(var(--nav-width) + 2rem)
            }

            .header {
                height: calc(var(--header-height) + 1rem);
                padding: 0 2rem 0 calc(var(--nav-width) + 2rem)
            }

            .header_img {
                width: 40px;
                height: 40px
            }

            .header_img img {
                width: 45px
            }

            .l-navbar {
                left: 0;
                padding: 1rem 1rem 0 0
            }

            .show {
                width: calc(var(--nav-width) + 156px)
            }

            .body-pd {
                padding-left: calc(var(--nav-width) + 188px)
            }
        }
    </style>

    </head>

    <body oncontextmenu='return false' class='snippet-body'>
    <body id="body-pd">
    <header class="header" id="header">
        <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
        <div class="header_img"> <img src="https://www.pngmart.com/files/21/Admin-Profile-Vector-PNG-File.png" alt=""> </div>
    </header>
    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> <a href="#" class="nav_logo"> <img src ="images/spnhs.png" style="width:1.5em; background-color:white; border-radius:100%;"> <span class="nav_logo-name">SPNHS</span> </a>
                <div class="nav_list">
                <a href="/admin-dashboard" class="nav_link" data-toggle="tooltip" data-placement="right" title="Dashboard"> <i class='bx bx-grid-alt nav_icon'></i> <span class="nav_name">Home</span></a>
                    <a href="/admin-news" class="nav_link" data-toggle="tooltip" data-placement="right" title="News"> <i class='bx bx-news nav_icon'></i> <span class="nav_name">News</span> </a>
                    <a href="/admin-event" class="nav_link active" data-toggle="tooltip" data-placement="right" title="Events"> <i class='bx bx-calendar-event nav_icon'></i> <span class="nav_name">Events</span> </a>
                    <a href="/admin-admission" class="nav_link" data-toggle="tooltip" data-placement="right" title="Programs"> <i class='bx bx-bookmark nav_icon'></i> <span class="nav_name">Programs</span> </a>
                    <a href="/admin-administration" class="nav_link" data-toggle="tooltip" data-placement="right" title="Administration"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Administration</span> </a>
                    <a href="/admin-archive" class="nav_link" data-toggle="tooltip" data-placement="right" title="About"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">About</span> </a>
                    <a href="/admin-contact" class="nav_link" data-toggle="tooltip" data-placement="right" title="Contact"> <i class='bx bx-phone nav_icon'></i> <span class="nav_name">Contact</span> </a>
                    <?php if($notif != 0)
                    echo '<a href="/admin-messages" class="nav_link" data-toggle="tooltip" data-placement="right" title="Messages"> <i class="bx bx-message nav_icon" style="color:red"></i> <span class="nav_name">Messages</span> </a>';
                    else echo '<a href="/admin-messages" class="nav_link" data-toggle="tooltip" data-placement="right" title="Messages"> <i class="bx bx-message nav_icon"></i> <span class="nav_name">Messages</span> </a>';
                    ?>
                    <a href="/admin-admin" class="nav_link" data-toggle="tooltip" data-placement="right" title="Admin"> <i class='bx bx-user-pin nav_icon'></i> <span class="nav_name">Admin</span> </a>
                </div>

            </div> <a href="main/logout" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
        </nav>
    </div>
    <!--Container Main start-->
    <div class="height-100 bg-light">
        <h4>Events</h4>

        @if(Session::get('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @endif

        @if(Session::get('fail'))
            <div class="alert alert-danger">
                {{Session::get('fail')}}
            </div>
        @endif

        @if ($list != null)
        <form action="main/editevents/{{ $list->ev_id }}" method="post" enctype="multipart/form-data"> 
        @csrf
        <div class="col-sm-12">      
            <label for="title" class="col-sm-4 col-form-label">Event</label> 
            <input type="text" class="form-control" id="sec" name="title" value="{{  $list->ev_title }}">

            <span style="color:red;"> @error ('title'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="title" class="col-sm-4 col-form-label">Date</label> 
            <input type="date" class="form-control" id="date" name="date" value="{{ $list->ev_date }}">

            <span style="color:red;"> @error ('date'){{$message}} @enderror </span>
        </div>


        <div class="col-sm-12">      
            <label for="time" class="col-sm-4 col-form-label">Start Time</label> 
            <input type="time" class="form-control" id="stime" name="stime" value="{{ $list->ev_stime }}">

            <span style="color:red;"> @error ('stime'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="time" class="col-sm-4 col-form-label">End Time</label> 
            <input type="time" class="form-control" id="etime" name="etime" value="{{ $list->ev_etime }}">

            <span style="color:red;"> @error ('etime'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="location" class="col-sm-4 col-form-label">Venue</label> 
            <input type="text" class="form-control" id="location" name="location" value="{{ $list->ev_location }}">

            <span style="color:red;"> @error ('location'){{$message}} @enderror </span>
        </div>

        <div class="col-sm-12">      
            <label for="img" class="col-sm-4 col-form-label">Cover Image</label> 
            <input type="file" class="form-control" id="image" name="image" value="{{ old ('image') }}">

            <span style="color:red;"> @error ('image'){{$message}} @enderror </span>
        </div>


        <div class="col-sm-12">      
            <label for="content" class="col-sm-4 col-form-label">Description</label> 
            <?php
             $content = strip_tags($list->ev_content);
            ?>
            <input type="content" class="form-control mb-3" id="content" name="content" value="{{ $content }}">
            <span style="color:red;"> @error ('content'){{$message}} @enderror </span>
        </div>

        <div class="float-right">
            <input type="reset" class="btn-reset mb-3 mt-0" name="reset">
            <button type="submit" class="btn-saves" name="save" >Save </button>
            <a href = "/admin-event"> Back </a>
        </div>

        </form>
        @endif
    </div>
    

    <!--Container Main end-->



    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src=''></script>
    <script type='text/javascript' src=''></script>
    
    <script type='text/Javascript'>document.addEventListener("DOMContentLoaded", function(event) {

    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId)

    // Validate that all variables exist
    if(toggle && nav && bodypd && headerpd){
    toggle.addEventListener('click', ()=>{
    // show navbar
    nav.classList.toggle('show')
    // change icon
    toggle.classList.toggle('bx-x')
    // add padding to body
    bodypd.classList.toggle('body-pd')
    // add padding to header
    headerpd.classList.toggle('body-pd')
    })
    }
    }

    showNavbar('header-toggle','nav-bar','body-pd','header')

    /*===== LINK ACTIVE =====*/
    const linkColor = document.querySelectorAll('.nav_link')

    function colorLink(){
    if(linkColor){
    linkColor.forEach(l=> l.classList.remove('active'))
    this.classList.add('active')
    }
    }
    linkColor.forEach(l=> l.addEventListener('click', colorLink))

    // Your code to run since DOM is loaded and ready
    });</script>

</body>
</html>