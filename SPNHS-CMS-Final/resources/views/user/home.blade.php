<!DOCTYPE html>
<html lang="en">
   <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Home</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/vendor/aos/aos.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
<body>


  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html"><img src ="/images/spnhs.png" style="margin-right:.5em;">SPNHS</a></h1>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="active" href="#">Home</a></li>
          <li><a href="news">News</a></li>
          <li><a href="events">Events</a></li>
          <li><a href="courses">Courses</a></li>
          <li><a href="administration">Administration</a></li>
          <li><a href="about">About</a></li>
          <li><a href="contact">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>

    </div>
  </header>

  


  @if ($cover != null)
    @foreach ($cover as $cover)
      <section id="hero" class="d-flex justify-content-center align-items-center" style="width: 100%; height: 80vh; background: url('/images/{{$cover->c_img}}') top center; background-size: cover; position: relative;">
    @endforeach
  @endif
    <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
      <h1>San Pedro National<br>High School</h1>
      <h2>WE HOLD THE KEY</h2>
    </div>
  </section>

  <main id="main">

  <section id="about" class="about">
      <div class="container" data-aos="fade-up">
      @if ($history != null)
          @foreach ($history as $history)
        <div class="row">
          <div class="col-lg-4 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="{{url('/images/'.$history->a_img)}}" class="img" alt="" style="width:300px;">
          </div>
          <div class="col-lg-8 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3>San Pedro National High School</h3>
            <p class="fst-italic">
            
            </p>
            <!-- <ul>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="bi bi-check-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul> -->
            <p>
            <?php echo $history->a_history;?>
            </p>

          </div>
        </div>
         @endforeach
        @endif
      </div>
    </section>

    <section id="counts" class="counts section-bg">
      <div class="container">
        <div class="row counters">
        @if ($info != null)
          @foreach ($info as $info)
          <div class="col-lg-4 col-4 text-center">
            <span data-purecounter-start="0" data-purecounter-end="{{$info->d_students}}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Students</p>
          </div>
    @endforeach
        @endif
        
        @if ($teacher != null)
          <div class="col-lg-4 col-4 text-center">
            <span data-purecounter-start="0" data-purecounter-end="{{$teacher}}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Teachers</p>
          </div>
      @endif

        @if ($countev != null)
        <div class="col-lg-4 col-4 text-center">
            <span data-purecounter-start="0" data-purecounter-end="{{$countev}}" data-purecounter-duration="1" class="purecounter"></span>
            <p>Events</p>
          </div>       
        @endif
        </div>
      </div>
    </section>

  
    <section id="why-us" class="why-us">
      <div class="container" data-aos="fade-up">
      @if ($about != null)
          @foreach ($about as $about)
        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>Why SPNHS?</h3>
              <p>
                <?php echo $about->a_why;?>
              </p>
              <div class="text-center">
                <a href="about" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          @endforeach
        @endif


          <div class="col-lg-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">

              @if ($event != null)
              @foreach ($event as $event)
              <?php
                $stime = date ('g:i A',strtotime($event->ev_stime));
                $etime = date ('g:i A',strtotime($event->ev_etime));
                    $date  = strtotime($event->ev_date);
                    $day   = date('d',$date);
                    $month = date('M',$date);
                    $year  = date('Y',$date);
              ?>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-calendar"></i>
                    <h4>{{$event->ev_title}}</h4>
                    {{$month}} {{$day}}, {{$year}} <br> {{$stime}}-{{$etime}}
                    {{$event->ev_location}} 
                  </div>
                </div>
              @endforeach
              @endif
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section>

    <section id="popular-courses" class="courses">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>News</h2>
          <p>Latest News</p>
        </div>

        <div class="row" data-aos="zoom-in" data-aos-delay="100">

        @if ($news != null)
          @foreach ($news as $news)
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="course-item">
              <img src="{{url('/images/'.$news->img)}}" class="img-fluid" alt="...">
              <div class="course-content">
                <h3><a href="/{{$news->slug}}">{{$news->title}}</a></h3>
                <p style="width: 250px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{strip_tags( $news->content)}}</p>
                <div class="trainer d-flex justify-content-between align-items-center">
                  <div class="trainer-profile d-flex align-items-center">
                    <p> Date published: {{$news->date}} </p>
                  </div>
                </div>
              </div>
            </div>
          </div> 
          @endforeach
        @endif
        </div>
      </div>
    </section>
  </main>

  <!-- ======= Footer ======= -->

  <footer id="footer">

<div class="container d-md-flex py-4">

  <div class="me-md-auto text-center text-md-start">
    <div class="copyright">
      &copy; Copyright <strong><span>Aisle Lush Valdez</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/ -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
  <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="https://www.facebook.com/SanPedroNationalHighSchool1964" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->


  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>


  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>


  <script src="assets/js/main.js"></script>

</body>

</html>