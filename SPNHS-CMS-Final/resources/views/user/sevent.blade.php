<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Events</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">


</head>

<body>
 
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html"><img src ="/images/spnhs.png" style="margin-right:.5em;">SPNHS</a></h1>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="news">News</a></li>
          <li><a class="active" href="events">Events</a></li>
          <li><a href="courses">Courses</a></li>
          <li><a href="administration">Administration</a></li>
          <li><a href="about">About</a></li>
          <li><a href="contact">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>

    </div>
  </header>

  <main id="main">
    
       

    <section id="popular-courses" class="courses">
      <div class="container" data-aos="fade-up">
        <div class="row" data-aos="zoom-in" data-aos-delay="100">
            @if ($event!=NULL)
                <div class="section-title mt-5">
                    <h2>Event</h2>
                    <p>{{$event->ev_title}}</p>
                    <?php
                    $stime = date ('g:i A',strtotime($event->ev_stime));
                    $etime = date ('g:i A',strtotime($event->ev_etime));
                        $date  = strtotime($event->ev_date);
                        $day1   = date('l',$date);
                        $day   = date('dS',$date);
                        $month = date('F',$date);
                        $year  = date('Y',$date);
                     ?>
                    <h6 style="color:grey;"> {{$day1}}, {{$month}} {{$day}} of {{$year}} <br>
                    {{$stime}} - {{$etime}}
                    at {{$event->ev_location}}
                    </h6>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="img mb-3" >
                            <img src="{{url('/images/'.$event->ev_img)}}" alt="..." style="width:500px;">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="overflow-auto">
                        @if ($list != null)
                        @foreach ($list as $item)
                        <?php
                        $stime = date ('g:i A',strtotime($item->ev_stime));
                        $etime = date ('g:i A',strtotime($item->ev_etime));
                            $date  = strtotime($item->ev_date);
                            $day1   = date('l',$date);
                            $day   = date('dS',$date);
                            $month = date('F',$date);
                            $year  = date('Y',$date);
                        ?>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-2"> <img src="{{url('/images/'.$item->ev_img)}}" alt="Image" style=" height: 35px;"/> </div>
                                            <div class="col-sm-10">     <a href="{{ action('App\Http\Controllers\UserController@viewSevent', [$item->ev_slug])}}" style="text-decoration: none;"> 
                                                <p style="font-weight:bold; font-size 9px;">
                                                    {{ $item->ev_title }}
                                                </p>    
                                                <p style="color:grey; margin-top:-1em; font-size 9px;">
                                                {{$day1}}, {{$month}} {{$day}} of {{$year}} <br>
                                                {{$stime}} - {{$etime}}
                                                at {{$event->ev_location}}
                                                </p>   
                                                </a> 
                                            </div>
                                        </div>
                                    </div>                                    
                              
                        @endforeach
                        @endif
                        </div>
                    </div>
                </div>

                <p>
                    <?php echo $event->ev_content;?>
                </p>
            @endif
        </div>
      </div>
    </section>

  </main>
    
    
  <!-- ======= Footer ======= -->

  <footer id="footer">

<div class="container d-md-flex py-4">

  <div class="me-md-auto text-center text-md-start">
    <div class="copyright">
      &copy; Copyright <strong><span>Aisle Lush Valdez</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mentor-free-education-bootstrap-theme/ -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
  <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="https://www.facebook.com/SanPedroNationalHighSchool1964" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->


  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <script src="assets/js/main.js"></script>

</body>

</html>