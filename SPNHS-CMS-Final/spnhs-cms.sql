-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 06:13 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spnhs-cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `a_id` int(11) NOT NULL,
  `a_vision` longtext DEFAULT NULL,
  `a_mission` longtext DEFAULT NULL,
  `a_corevalues` longtext DEFAULT NULL,
  `a_why` longtext DEFAULT NULL,
  `a_history` longtext DEFAULT NULL,
  `a_img` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`a_id`, `a_vision`, `a_mission`, `a_corevalues`, `a_why`, `a_history`, `a_img`) VALUES
(16, '<p>We dream of Filipinos<br />\r\nwho passionately love their country<br />\r\nand whose values and competencies<br />\r\nenable them to realize their full potential<br />\r\nand contribute meaningfully to building the nation.</p>\r\n\r\n<p>As a learner-centered public institution,<br />\r\nthe Department of Education<br />\r\ncontinuously improves itself&nbsp;&nbsp;<br />\r\nto better serve its stakeholders.</p>', '<p>To protect and promote the right of every Filipino to quality, equitable, culture-based, and complete basic education where:</p>\r\n\r\n<p><strong>Students</strong> learn in a child-friendly, gender-sensitive, safe, and motivating environment.<br />\r\n<strong>Teachers</strong> facilitate learning and constantly nurture every learner.<br />\r\n<strong>Administrators and staff</strong>, as stewards of the institution, ensure an enabling and supportive environment for effective learning to happen.<br />\r\n<strong>Family, community, and other stakeholders</strong> are actively engaged and share responsibility for developing life-long learners.</p>', '<blockquote>\r\n<p><em>Maka-Diyos<br />\r\nMaka-tao<br />\r\nMakakalikasan<br />\r\nMakabansa</em></p>\r\n</blockquote>', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil.</p>', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem.Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit, tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit, quia.</p>\r\n\r\n<p>Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit, tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit, quia.</p>', '1654924686-.png');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `add_id` int(11) NOT NULL,
  `add_street` varchar(50) NOT NULL,
  `add_city` varchar(50) NOT NULL,
  `add_province` varchar(50) NOT NULL,
  `add_zip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`add_id`, `add_street`, `add_city`, `add_province`, `add_zip`) VALUES
(2, 'San Pedro', 'Moncada', 'Tarlac', '2308');

-- --------------------------------------------------------

--
-- Table structure for table `cover`
--

CREATE TABLE `cover` (
  `c_id` int(11) NOT NULL,
  `c_img` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cover`
--

INSERT INTO `cover` (`c_id`, `c_img`) VALUES
(14, '1655365784-.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` varchar(50) NOT NULL DEFAULT '1',
  `d_students` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `d_students`) VALUES
('1', '1000000');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `e_id` int(11) NOT NULL,
  `e_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`e_id`, `e_email`) VALUES
(4, 'spnhs@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `ev_id` int(11) NOT NULL,
  `ev_title` longtext NOT NULL,
  `ev_stime` time NOT NULL,
  `ev_etime` time NOT NULL,
  `ev_date` date NOT NULL,
  `ev_content` longtext NOT NULL,
  `ev_slug` longtext NOT NULL,
  `ev_img` longblob NOT NULL,
  `ev_location` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`ev_id`, `ev_title`, `ev_stime`, `ev_etime`, `ev_date`, `ev_content`, `ev_slug`, `ev_img`, `ev_location`) VALUES
(4, 'Buwan ng Wika', '08:00:00', '17:00:00', '2022-09-25', 'Halamanhalaman haHalamanhalaman haHalamanhalaman haHalamanhalaman haHalamanhalaman ha', 'buwan-ng-wika2022-09-25', 0x313635353237313430362d2e6a7067, 'San Pedro Plazuela'),
(5, 'Nutrition Month', '07:30:00', '12:00:00', '2022-03-10', 'afasffasfasf', 'nutrition-month', 0x313635353237313636302d2e6a7067, 'Moncada, Tarlac'),
(6, 'Sci-Math', '08:00:00', '12:00:00', '2022-06-15', 'arqwreqrrqrujtu', 'sci-math2022-06-15', 0x313635353237313833342d2e706e67, 'SPNHS Covered Court');

-- --------------------------------------------------------

--
-- Table structure for table `hteacher`
--

CREATE TABLE `hteacher` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `ranking` varchar(50) NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '0',
  `img` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hteacher`
--

INSERT INTO `hteacher` (`id`, `name`, `ranking`, `subject`, `img`) VALUES
(2, 'Danilo A. Balbas Jr., PhD.', 'Head Teacher II', 'Science', '1655106354-.jpg'),
(3, 'Wella A. Cariño', 'Head Teacher I', 'Mathematics', '1655106517-.jpg'),
(4, 'Chris Darwin T. Dela Cruz', 'Head Teacher I', 'Music, Arts, Physical Education, and Health (MAPEH)', '1655106568-.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `content`, `name`, `date`, `email`, `status`) VALUES
(3, 'Message', 'Aisle Valdez', '2022-06-20', 'eaxlaisle@gmail.com', 1),
(8, 'asdasdsa', 'Aisle Lush Santos Valdez', '2022-07-06', 'asass@jnad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2022_05_22_232753_create_users_table', 1),
(3, '2022_05_30_013653_create_events_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` longtext NOT NULL,
  `slug` longtext NOT NULL,
  `content` longtext NOT NULL,
  `img` longblob NOT NULL,
  `date` date NOT NULL,
  `last_edit` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `img`, `date`, `last_edit`) VALUES
(19, 'Bongbong Marcos: China PH’s ‘strongest partner’ but independent foreign policy to stay', 'bongbong-marcos-china-phs-strongest-partner-but-independent-foreign-policy-to-stay', '<p>MANILA, Philippines &mdash; While calling China a &ldquo;good friend&rdquo; and the Philippines&rsquo; &ldquo;strongest partner&rdquo;, President-elect Ferdinand &ldquo;Bongbong&rdquo; Marcos Jr. maintained on Friday (June 10) that he will implement an &ldquo;independent foreign policy.&rdquo;</p>\r\n\r\n<p>Marcos said this at an event organized by the Association for Philippines-China Understanding where he stressed the longstanding relationship between the two countries.</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', 0x313635343837333832342d2e6a7067, '2022-06-10', NULL),
(20, 'The Department of Education and Microsoft launch Reading Progress tool to 23 million Filipino Students in celebration of National Reading Month', 'the-department-of-education-and-microsoft-launch-reading-progress-tool-to-23-million-filipino-students-in-celebration-of-national-reading-month', '<p><strong>Manila, Philippines, December 1, 2021</strong> &ndash; The Department of Education (DepEd) and Microsoft recently concluded a month-long series of virtual events for students across the Philippines throughout November, in celebration of National Reading Month. The program introduced Microsoft&rsquo;s Reading Progress as part of a shared effort to drive literacy through reading comprehension for Filipino students.&nbsp;</p>\r\n\r\n<p>&ldquo;The National Reading Month serves as a reminder to us in light of our performance that we must double our efforts to close the learning gap, particularly&nbsp;in the area of&nbsp;reading, which is the cornerstone of learning,&rdquo; said DepEd Secretary Leonor&nbsp;Magtolis&nbsp;Briones.&nbsp;&nbsp;</p>\r\n\r\n<p>Microsoft&rsquo;s&nbsp;Reading Progress&nbsp;is an application built&nbsp;into&nbsp;Microsoft Teams which is accessible for free to 23 million students and 950,000 teachers within the Department of&nbsp;Education&rsquo;s&nbsp;nationwide system. Reading Progress is an AI-powered tool designed to support and track students&rsquo; reading fluency, specifically reading accuracy, speed, and pronunciation. Students can record themselves through Reading Progress and submit their reading assignments to teachers.&nbsp;Teachers&nbsp;can assess their&nbsp;students&rsquo;&nbsp;reading progress through an automatically generated report. The tool is for K to 12 learners and&nbsp;has accessibility functions to accommodate&nbsp;those who have dyslexia and other learning challenges.&nbsp;</p>\r\n\r\n<p>&ldquo;Technology has been at the forefront of the world&rsquo;s response to COVID-19, whether in healthcare, economic recovery, building&nbsp;digital skills&nbsp;or in this case, sustaining learning experiences for students of all ages,&rdquo; said Microsoft Philippines Public Sector Director, Joanna Rodriguez.&nbsp;&ldquo;We are committed to supporting the Department of Education&rsquo;s many efforts to empower both learners and educators during this challenging time and to continue delivering innovation and lasting transformation for the Philippines.&rdquo;</p>\r\n\r\n<p>This year&rsquo;s National Reading Month&nbsp;was&nbsp;themed&nbsp;<em>&ldquo;Bawat&nbsp;Bata&nbsp;Bumabasa&nbsp;Sa Kabila ng Hamon ng&nbsp;Pandemya,&rdquo;&nbsp;(</em>Every Child Reading Despite the Challenge of the Pandemic).&nbsp;With ongoing limitations to in-person events and face-to-face classes, schools and learning centers nationwide&nbsp;participated virtually in DepEd&rsquo;s series of virtual activities throughout the month of November aimed to promote reading, improve&nbsp;literacy&nbsp;and upskill educators with innovative&nbsp;technology&nbsp;solutions.&nbsp; &nbsp;</p>\r\n\r\n<p>DepEd conducted weekly reading sessions with reading ambassadors<em>&nbsp;</em>Secretary Leonor Briones, Undersecretary and Chief of Staff Nepomuceno Malaluan,&nbsp;Undersecretary for Curriculum &amp; Instruction Diosdado San Antonio, Undersecretary for Finance Annalyn Sevilla, Undersecretary for External Partnership&nbsp;Tonisito&nbsp;Umali,&nbsp;Director for&nbsp;Bureau of Learning Delivery Leila Areola, and Microsoft&rsquo;s Public Sector Director Joanna Rodriguez,&nbsp;who read award-winning Filipino literary pieces.&nbsp;&nbsp;</p>\r\n\r\n<p>Capacity building sessions on effective story reading and use of Reading Progress and Microsoft Teams were conducted to help parents and teachers.&nbsp;</p>\r\n\r\n<p>The Reading Progress tool was featured during the&nbsp;<em>Reading Cup</em>, a two-week gamified regional reading contest that assessed the reading fluency levels of students from Grades 7-12 and recognized exemplary&nbsp;readers. The culminating activity was held on&nbsp;November&nbsp;29&nbsp;with the&nbsp;<em>Araw ng Pag-Basa Challenge</em>, where students and teachers nationwide undertook a synchronized reading activity led by Secretary Briones.&nbsp;&nbsp;</p>\r\n\r\n<p>&ldquo;Through Reading&nbsp;Progress, learners&nbsp;can&nbsp;build reading skills through proactive, independent&nbsp;reading.&nbsp;At the same time, teachers&nbsp;are able to&nbsp;track the progress of their students&rsquo;&nbsp;reading capabilities&nbsp;more easily&nbsp;and efficiently.&nbsp;Again,&nbsp;we are demonstrating that&nbsp;Bayanihan&nbsp;is alive, and thank you once again, Microsoft Philippines, for doing your share in this effort to ensure that children become fluent readers,&rdquo; said DepEd Undersecretary&nbsp;Diosdado M. San Antonio.&nbsp;</p>\r\n\r\n<p>Moving forward,&nbsp;DepEd and Microsoft&nbsp;will continue their efforts to make reading an essential part of the lives of millions of Filipino students.&nbsp;&ldquo;Let us celebrate our reading month, not just for a month but make it a habit for our learners to read and enjoy growing up with books,&rdquo;&nbsp;said Secretary Briones.&nbsp;&nbsp;</p>', 0x313635343837333932312d2e6a7067, '2022-06-10', '2022-06-16'),
(21, 'Philippines protests swarm of 100 Chinese vessels in reef', 'philippines-protests-swarm-of-100-chinese-vessels-in-reef', '<p>MANILA, Philippines &mdash; The Philippines has protested the return of over 100 Chinese vessels illegally operating in the waters in and around Julian Felipe Reef last April 4, barely a year after the same swarming incident was protested by the government, the Department of Foreign Affairs (DFA) said yesterday.</p>\r\n\r\n<p>Julian Felipe Reef is a low-tide elevation within the territorial sea of relevant high-tide features in the Kalayaan Island Group, including Chigua Reef, over which the Philippines has sovereignty.</p>\r\n\r\n<p>&ldquo;The lingering unauthorized presence of Chinese fishing and maritime vessels is not only illegal, but is also a source of instability in the region,&rdquo; the DFA said in a statement.</p>', 0x313635343837333937302d2e77656270, '2022-06-10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `number`
--

CREATE TABLE `number` (
  `num_id` int(11) NOT NULL,
  `num_number` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `number`
--

INSERT INTO `number` (`num_id`, `num_number`) VALUES
(3, '09123456789');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `principal`
--

CREATE TABLE `principal` (
  `name` varchar(50) DEFAULT NULL,
  `ranking` varchar(50) DEFAULT NULL,
  `img` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `principal`
--

INSERT INTO `principal` (`name`, `ranking`, `img`) VALUES
('Marylene I. Antalan, EdD.', 'Principal IV', '1655105562-.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `p_id` int(11) NOT NULL,
  `p_name` longtext NOT NULL,
  `p_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`p_id`, `p_name`, `p_level`) VALUES
(5, 'STE', 'Junior High School'),
(6, 'General Academic Strand', 'Senior High School'),
(7, 'Science, Technology, Engineering, Mathematics (STEM)', 'Senior High School'),
(8, 'Accountancy Business and Management (ABM)', 'Senior High School'),
(9, 'Humanities and Social Sciences (HUMSS)', 'Senior High School');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ranking` varchar(50) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `img` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `name`, `ranking`, `subject`, `img`) VALUES
(1, 'Andrew Valle', 'Teacher III', 'Mathematics', '1655107101-.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '2022-05-22 16:00:13', '2022-05-22 16:00:13'),
(2, 'Aislevee', 'password', NULL, NULL),
(3, 'admin123', 'password', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`add_id`);

--
-- Indexes for table `cover`
--
ALTER TABLE `cover`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`ev_id`);

--
-- Indexes for table `hteacher`
--
ALTER TABLE `hteacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `number`
--
ALTER TABLE `number`
  ADD PRIMARY KEY (`num_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `add_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cover`
--
ALTER TABLE `cover`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `ev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hteacher`
--
ALTER TABLE `hteacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `number`
--
ALTER TABLE `number`
  MODIFY `num_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
