<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'App\Http\Controllers\MainController@index');
Route::get('/sign-up', 'App\Http\Controllers\MainController@signup');
Route::post('/adduser', 'App\Http\Controllers\MainController@adduser');
Route::post('/dashboard', 'App\Http\Controllers\MainController@userlogin');
Route::get('/dashboard/{id}', 'App\Http\Controllers\MainController@dashboard');
Route::post('appointment', 'App\Http\Controllers\MainController@appointment');
Route::post('cancel', 'App\Http\Controllers\MainController@cancel');
Route::post('logout', 'App\Http\Controllers\MainController@logout');
Route::get('admin-login', 'App\Http\Controllers\MainController@logAdmin');
Route::post('admindashboard', 'App\Http\Controllers\MainController@adminlogin');
Route::get('/admin/{id}', 'App\Http\Controllers\MainController@admin');
Route::get('/adminLogout', 'App\Http\Controllers\MainController@logoutAdmin');
Route::post('/done', 'App\Http\Controllers\MainController@done');