<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

    <script type='text/javascript' src=''></script>

    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");

    </style>
    </head>

    <style>
        .circle {
        border-radius: 50%;
        width: 34px;
        height: 34px;
        padding-top: 4px;
        padding-left: 5px;
        background-color:#0275d8;
        color:white;
        font-weight:bold;
        }

        .circle1 {
        border-radius: 50%;
        width: 34px;
        height: 34px;
        padding-top: 4px;
        padding-left: 11px;
        background-color:#0275d8;
        color:white;
        font-weight:bold;
        }

        p{
            color:#0275d8;
            font-weight:bold;
        }

        </style>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="navbar-brand mt-2 mt-lg-0">
                <p> APPOINTMENT </P>
            </div>
 
            </div>

            <div class="d-flex align-items-center">

                <div class="circle"> AD </div>

                <form action="/adminLogout" method="post"> 
                    @csrf
                <button type="submit" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
                </form>
     
        
              
           
            </div>

            
 
        </div>

        </nav>

        <div class="container">
            @if ($list1 != null)
            @foreach ($list1 as $item)
                    <?php
                        $date  = strtotime($item->date);
                        $day   = date('d',$date);
                        $month = date('M',$date);
                        $year  = date('Y',$date);
                    ?>
                <div class="card" style="margin-top:1em;">
                    <div class="card-body" id="card-center">
                        <div class="container">
                            <div class="row">
                                <?php

                                $user= $item -> userid;
                                $name = DB::table('users')->where('id',$user)->pluck('name');

                                $var = str_replace("[", "", $name);
                                $var2 = str_replace("]", "", $var);
                                $var3 = str_replace('"', "", $var2);                             
                                $firstStringCharacter = substr($var3, 0, 1);
                                $string = strtoupper($firstStringCharacter);
                                ?>
                                <div class="col-sm-1"> <div class="circle1" style="margin-top:.5em;">  {{$string}}</div> 
                                    
                                </div>
                                <div class="col-sm-6">   

                                    <h4>
                                        
                                        {{$var3}}
                                    </h4> 
                         
                                    <p style="color:grey; margin-top:.5em;">

                                        Date:  {{$month." ".$day.", ".$year}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                                        Time: {{$item->time}} </p>

                                        <p >

                                        Purpose:  {{$item->concern}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                                        </p>
                                </div>
                                <div class="col-sm-3 mt-3">
                                        <?php
                                            if ($item->status == '0'){
                                                echo  " <p style='color:orange;'>  Status: Approved  </p>";
                                            } else if ($item->status == '1'){
                                                echo " <p style='color:red;'>  Status: Cancelled  </p>";
                                            } else 
                                                echo " <p style='color:green;'>  Status: Done  </p>";
                                                                                 
                                            ?>

                                
                                </div>
                                <div class="col-sm-2">
                                @if ($item->status != 0)
                                <form action="/done" method="post" enctype="multipart/form-data"> 
                                    @csrf
                                <input type="hidden" name="id" id="id" value="{{ $item -> app_id}}" />
                                <button type="submit" class="btn btn-dark"  disabled>Done</button>
                                </form>

                            
                                @else
                               
                                <form action="/done" method="post" enctype="multipart/form-data"> 
                                    @csrf
                                <input type="hidden" name="id" id="id" value="{{ $item -> app_id}}" />
                                <button type="submit" class="btn btn-primary" >Done</button>
                                </form>
                           
                                @endif
                                        </div>
                            </div>
                        </div>                                    
                    </div>  
                </div>
            @endforeach
            @endif
                                        </div>
            </div>


    </body>
</html>