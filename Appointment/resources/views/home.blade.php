<!doctype html>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

    <script type='text/javascript' src=''></script>

    <style>@import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");

    </style>
    </head>

    <style>
        .circle {
        border-radius: 50%;
        width: 34px;
        height: 34px;
        padding-top: 4px;
        padding-left: 11px;
        background-color:#0275d8;
        color:white;
        font-weight:bold;
        }

        p{
            color:#0275d8;
            font-weight:bold;
        }

        </style>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="navbar-brand mt-2 mt-lg-0">
                <p> APPOINTMENT </P>
            </div>
 
            </div>

            <div class="d-flex align-items-center">


        
                <?php
                    $uname = request()->segment(2);
                    $firstStringCharacter = substr($uname, 0, 1);
                    $string = strtoupper($firstStringCharacter);
                   
                    ?> 
                <div class="circle"> {{ $string }} </div>

                <form action="/logout" method="post"> 
                    @csrf
                <button type="submit" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-log-out"></span> Log out
                </button>
                </form>
     
              
           
            </div>
 
        </div>

        </nav>


        <div class="row">

        <div class="col-lg">
        <div class="container">
            @if(Session::get('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            @endif

            @if(Session::get('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
            @endif
            <h3> New Appointments </h3>

            <form action="/appointment" method="post" enctype="multipart/form-data"> 

            @csrf
                <?php
                    $uname = request()->segment(2);
                ?>
                <input type="hidden" name="id" id="name" value="<?php echo $uname ?>" />

                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Concern</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name ="concern" value="{{ old ('concern') }}">
                    <span style="color:red;"> @error ('concern'){{$message}} @enderror </span>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Date</label>
                    <input type="date" class="form-control" id="exampleInputPassword1" name="date" value="{{ old ('date') }}">
                    <span style="color:red;"> @error ('date'){{$message}} @enderror </span>
                </div>


                <div class="mb-3">
                <label for="disabledSelect" class="form-label">Time</label>
                <select id="disabledSelect" class="form-select" name="time">
                        <option value="" selected disabled hidden>Choose times</option>
                        @foreach ($list as $item)
                        <option value="{{ $item->id }}">{{ $item->time }}</option>
                        @endforeach
                </select>
                <span style="color:red;"> @error ('time'){{$message}} @enderror </span>
                </div>

                <button type="submit" class="btn btn-primary mb-5">Submit</button>

            </form>
            </div>
        </div>

        <div class="col-lg">

            <div class="container">
            @if(Session::get('success1'))
                <div class="alert alert-success">
                    {{Session::get('success1')}}
                </div>
            @endif

            <h3> My Appointments </h3>

             @if ($list1 != null)
            @foreach ($list1 as $item)
                    <?php
                        $date  = strtotime($item->date);
                        $day   = date('d',$date);
                        $month = date('M',$date);
                        $year  = date('Y',$date);
                    ?>
                <div class="card" style="margin-top:1em;">
                    <div class="card-body" id="card-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-1"> <div class="circle" style="margin-top:.5em;"> {{ $string }} </div> 
                                    
                                </div>
                                <div class="col-sm-6">    
                                    <p style="font-weight:bold;">
                                    {{$month." ".$day." ".$year}}
                                    </p>  
                                
                                    <p style="color:grey; margin-top:-1em;">

                                
                                        Time: {{$item->time}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </p>
                                </div>
                                <div class="col-sm-5 mt-3">
                                        <?php
                                            if ($item->status == '0'){
                                                echo  " <p style='color:orange;'>  Status: Approved  </p>";
                                            } else if ($item->status == '1'){
                                                echo " <p style='color:red;'>  Status: Cancelled  </p>";
                                            } else 
                                                echo " <p style='color:green;'>  Status: Done  </p>";
                                                                                 
                                            ?>

                                
                                </div>
                                @if ($item->status != 0)
                                <button type="submit" class="btn btn-primary"  disabled>Cancel</button>

                                @else
                               
                                <form action="/cancel" method="post" enctype="multipart/form-data"> 
                                    @csrf
                                <input type="hidden" name="id" id="id" value="{{ $item -> app_id}}" />
                                <button type="submit" class="btn btn-primary" style="width:100%;">Cancel</button>
                                </form>
                           
                                @endif
                                
                            </div>
                        </div>                                    
                    </div>  
                </div>
            @endforeach
            @endif

            </div>
        </div>

            
        
        </div>

    </body>
</html>