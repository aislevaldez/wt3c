<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MainController extends Controller
{
    function index(){

        return view('login');
    }

    function signup(){

        return view('signup');
    }

    function logout(){

        return view('login');
    }

    function logoutAdmin(){

        return view('admin/login');
    }

    function adduser(Request $request){
        $request -> validate([
            'name' => 'required',
            'uname' => 'required|unique:users,uname|regex:/(^[a-zA-Z]+[a-zA-Z0-9\\-]*$)/u',
            'email' => 'required|email|unique:users,email',
            'pass' => 'required|min:8',
        ]);

        $query = DB::table('users')->insert([
            'name'=> $request-> input('name'),
            'uname'=> $request-> input('uname'),
            'email'=> $request-> input('email'),
            'password' =>  $request-> input('pass'),
        ]);

        if($query){
            return back()->with('success', 'Successfuly Registered');
        } else
            return back()->with('fail', 'Repeat Registration');
        // return $request -> input();
    }


    function adminlogin(Request $request){ 
        $request -> validate([      
            'your_name' => 'required|regex:/(^[a-zA-Z]+[a-zA-Z0-9\\-]*$)/u',
            'your_pass' => 'required|min:8',
        ]);

        $uname = $request-> input('your_name');
        $pass = $request-> input('your_pass');

        $user = DB::table('admin')->where('username',$uname)->
        where('password', $pass)->pluck('username');        

        if($user){

           
              $var = str_replace("[", "", $user);
              $var2 = str_replace("]", "", $var);
              $var3 = str_replace('"', "", $var2);

            // return redirect()->route('dashboard',['id' => $id]);
            return redirect('/admin/'.$var3);
            
        } else
            return back()->with('fail', 'Invalid Login Details');

    }



    function userlogin(Request $request){ 
        $request -> validate([      
            'your_name' => 'required|regex:/(^[a-zA-Z]+[a-zA-Z0-9\\-]*$)/u',
            'your_pass' => 'required|min:8',
        ]);

        $uname = $request-> input('your_name');
        $pass = $request-> input('your_pass');

        $user = DB::table('users')->where('uname',$uname)->
        where('password', $pass)->pluck('uname');        

        if($user){

           
              $var = str_replace("[", "", $user);
              $var2 = str_replace("]", "", $var);
              $var3 = str_replace('"', "", $var2);

            // return redirect()->route('dashboard',['id' => $id]);
            return redirect('/dashboard/'.$var3);
            
        } else
            return back()->with('fail', 'Invalid Login Details');

    }
    



    function dashboard(){
              
        $uname = request()->segment(2);

        $user = DB::table('users')->where('uname',$uname)->pluck('id');

            $var = str_replace("[", "", $user);
            $var2 = str_replace("]", "", $var);
            $var3 = str_replace('"', "", $var2);


        if($user){

            $data = array(
                'list'=>DB::table('time')->get()
            );

            $data1 = array(
            'list1' => DB::table('appointment')
            ->join('time', 'appointment.time', '=', 'time.id')
            ->where('appointment.userid',$var3)
            ->orderBy('app_id', 'DESC')
            ->get()
            );

            return view('home',$data,$data1);
        }
        else return redirect('login');       
    }
    

    function appointment(Request $request){

        $today = date("m/d/Y");

        $request -> validate([      
            'date' => 'required|after:today',
            'time' => 'required',
            'concern' => 'required',
        ]);

        $uname = $request -> input ('id');

        $user = DB::table('users')->where('uname',$uname)->pluck('id');

                $var = str_replace("[", "", $user);
                $var2 = str_replace("]", "", $var);
                $var3 = str_replace('"', "", $var2);

                $date = $request-> input('date');
                $time =  $request-> input('time');

              $data =DB::table('appointment')->where('date',$date)->where('time', $time)->where('status',0)->first();
            
                    if($data){
                        return back()->with('fail', 'The slot is already taken');
                    } 
                    else{
                        $query = DB::table('appointment')->insert([
                            'userid'=> $var3,
                            'concern'=> $request-> input('concern'),
                            'date'=> $date,
                            'time'=> $time,
                        ]);
            
                            return back()->with('success', 'Appointment Submmitted');
                    }

                }

        function cancel(Request $request){
            $id = $request-> input('id');

            $query = DB::table('appointment')
            ->where('app_id', $id)
            ->update([
                'status' => '1',
            ]);

            return back()->with('success1', 'Appointment Cancelled');
        }   
        
        function admin(){

            $data1 = array(
                'list1' => DB::table('appointment')
                ->join('time', 'appointment.time', '=', 'time.id')
                ->orderBy('date', 'DESC')
                ->get()
                );

            return view('admin/admin',$data1);

            

        }

        function logAdmin(){
            return view('admin/login');
        }


        function done (Request $request){

            $id = $request-> input('id');

            $query = DB::table('appointment')
            ->where('app_id', $id)
            ->update([
                'status' => '2',
            ]);

            return back()->with('success1', 'Done');

        }
}
