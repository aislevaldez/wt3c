<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customer');
});

// Route::get('/item', [OrderController::class, 'itemDisplay' ]);
Route::get('/item/{itemno}/{name}/{price}', 'App\Http\Controllers\OrderController@itemDisplay');

Route::get('/customer/{custnum}/{name}/{address}', 'App\Http\Controllers\OrderController@customerDisplay');

Route::get('/order/{custnum}/{name}/{orderno}/{date}', 'App\Http\Controllers\OrderController@orderDisplay');

Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{quantity}', 'App\Http\Controllers\OrderController@orderdetailsDisplay');
