<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function itemDisplay($itemno, $name, $price)  
  {  
    return view('item') -> with ('itemno', $itemno) -> with ('name', $name) -> with ('price', $price);
  }   

  public function customerDisplay($custnum, $name, $address)  
  {  
    return view ('customer') -> with ('custnum', $custnum) ->with ('name', $name) -> with ('address', $address);
  }   

  public function orderDisplay($custnum, $name, $orderno, $date)  
  {  
    return view ('order') -> with ('custnum', $custnum) ->with ('name', $name) -> 
    with ('orderno', $orderno) -> with ('date', $date);
  }   

  public function orderdetailsDisplay($transno, $orderno, $itemid, $name, $price, $quantity)  
  {  
    return view ('orderdetails') -> with ('transno', $transno) -> with ('orderno', $orderno) -> 
    with ('itemid', $itemid) ->with ('name', $name) -> with ('price', $price)  -> with ('quantity', $quantity);
  }   

}


