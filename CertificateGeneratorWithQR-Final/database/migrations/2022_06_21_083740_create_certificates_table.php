<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->id('id')->autoIncrement();
            $table->bigInteger('training_id');
            $table->bigInteger('certificate_id');
            $table->string('name');
            $table->string('description');
            $table->string('organizer');
            $table->string('position');
            $table->string('status');
            $table->date('email_send_date_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
};
